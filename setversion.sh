#!/bin/bash

INIT_FILES="graphbot/__init__.py"
DOC_FILES="doc/source/conf.py"

echoerr(){
    >&2 echo $1
}

usage(){
    echoerr "$0 <version>"
}

check_clean_wd(){
    git describe --always --tags --dirty=+ | grep + > /dev/null
    if [[ "$?" -eq "0" ]]
    then
        echoerr "The working directory is dirty. Aborting."
        exit 1
    fi
}

major_version(){
    echo $1 | sed -r 's/^([^\.]+(\.[^\.]+)?).*/\1/'
}

change_init(){
    for f in ${INIT_FILES}
    do
        sed -i -r -e "s/^(__version__ *= *)(.*)/\1'$1'/" $f
    done
}

change_docconf(){

    release=$1
    version=$(major_version ${release})

    for f in ${DOC_FILES}
    do
        sed -i -r -e "s/^(version *= *)(.*)/\1'${version}'/" \
                  -e "s/^(release *= *)(.*)/\1'${release}'/" \
                  $f
    done
}

change_version(){
    echo "$1" > VERSION
}

if [ -z "$1" ]
  then
    usage
    exit 1
fi

check_clean_wd &&
make test &&
change_version $1 &&
change_init $1 &&
# change_docconf $1 &&
# make doc &&
# git co -b pre_version-$1 &&
git checkout -b v$1_cand &&
git add --all &&
git commit -m "version set to $1" &&
echoerr "PROCESO EXITOSO. Nueva versión: $1"
