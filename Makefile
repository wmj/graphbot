PYTHON := python
SETUP := setup.py


.PHONY: all clean

all:

flake8-py2: flake8.awk
	`which python2` -m flake8 . | awk -f $< >&2

flake8-py3: flake8.awk
	`which python3` -m flake8 . | awk -f $< >&2

flake8: flake8.awk
	flake8 . | awk -f $< >&2

test: $(SETUP) tox.ini
	tox

clean: clean-pyco clean-cache clean-pycache

clean-pyco:
	@-find . -name "*.py[co]" -type f -delete

clean-cache:
	@-find . -name ".cache" -type d -ls -exec rm -rv {} \;

clean-pycache:
	@-find . -name "__pycache__" -type d -ls -exec rm -rv {} \;

clean-version:
	-rm -fr build
	-rm -fr *.egg-info

clean-dist:
	-rm -fr dist

sdist: $(SETUP) clean clean-version
	$(PYTHON) $< sdist

bdist: $(SETUP) clean clean-version
	$(PYTHON) $< bdist bdist_wheel --universal
