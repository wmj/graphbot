
graphbot
========

Bot de análisis de secciones requeridas para sitios MediaWiki.

La aplicación reconstruye el grafo de enlaces entre secciones entre las páginas
del sitio analizado. Dado la carga y el coste (es necesario expandir cada página)
de consultas a realizar, el análisis se realiza exclusivamente sobre un volcado
``XML`` del sitio.

Actualmente está personalizado para explorar `es.wiktionary.org`_


Configuración inicial
---------------------

El bot usa el framework `pywikibot`_ para obtener la configuración del sitio
en exploración, por lo que es necesario crear un fichero `user-config.py`_

Como ejemplo, para explorar `es.wiktionary.org`_, crearemos un archivo de acceso
anónimo en el directorio de trabajo con el siguiente contenido:

.. code-block:: python

    # -*- coding: utf-8 -*-
    family = 'wiktionary'
    mylang = 'es'

A continuación, obtenemos un volcado de la wiki sobre el que se va a realizar
el análisis. Puedes descargarlo de `dumps.wikimedia.org`_. Es necesario que
el volcado contenga al menos artículos y plantillas.

.. code-block:: bash

    wget https://dumps.wikimedia.org/eswiktionary/20171020/eswiktionary-20171020-pages-articles.xml.bz2


Barrido
-------

Consideraciones antes de la exploración
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- El proceso inicial puede tardar varios días en completarse. Puedes usar `pypy`_
  para obtener una ganancia en el tiempo de cómputo.
- Para mejorar el tiempo de proceso, la aplicación cachea múltiples resultados
  intermedios, por lo que es buena idea contar con un tamaño generoso de RAM.
- La información recopilada se almacena en una base de datos `sqlite3`_ que
  acabará teniendo un tamaño del orden del gigabyte.


Barrido inicial
~~~~~~~~~~~~~~~

.. code-block:: bash

    graphbot run <volcado xml> [<db>]

En nuestro caso:

.. code-block:: bash

    graphbot run eswiktionary-20171020-pages-articles.xml.bz2

Proceso
^^^^^^^

Con el comando anterior, el bot realiza varias tareas consecutivas:

1. Almacena la información de contexto: cuerpo de las plantillas, nombres de
   páginas y fechas.

   ::

       graphbot step_context <volcado xml> [<db>]

2. Expande las páginas y almacena las secciones y enlaces a otras páginas y
   secciones.

   ::

       graphbot step_collect <volcado_xml> [<db>]

3. Crea un hash de semejanza en los títulos de páginas para obtener información
   de desambiguación.

   ::

      graphbot collect_confusables [<db>]

Barrido incremental
~~~~~~~~~~~~~~~~~~~

Si se quiere actualizar el análisis a un volcado más reciente, es buena idea
realizar una exploración solamente en la información que se ha cambiado, por
la ganancia en tiempo de cómputo.

.. code-block:: bash

    graphbot run --incremental <nuevo volcado xml> [<db análisis previo>]


En nuesto caso, si queremos actualizar a un nuevo volcado con fecha ``20171120``:

.. code-block:: bash

    graphbot run --incremental eswiktionary-20171120-pages-articles.xml.bz2


Proceso incremental
^^^^^^^^^^^^^^^^^^^

Con el comando anterior, el bot realiza varias tareas consecutivas:

1. Almacena la información de contexto: cuerpo de las plantillas, nombres de
   páginas y fechas.

   ::

       graphbot step_context --incremental <volcado xml> [<db>]

2. Genera un índice de transclusiones entre páginas.

   ::

       graphbot step_transclusion <volcado xml> [<db>]

3. Propaga la fecha de actualización de cada página. Esta fecha será la fecha
   más reciente de entre la página y las páginas transcluidas en ella.

   ::

       graphbot step_propagate [<db>]

4. Genera un índice de las páginas con fecha de actualización posterior a la
   última actualización realizada en el análisis del último volcado.

   ::

       graphbot build_index [<db>]

5. Expande las páginas contenidas en el índice y almacena las secciones y enlaces a
   otras páginas y secciones.

   ::

      graphbot step_collect --incremental <volcado_xml> [<db>]


6. Recrea el hash de semejanza en los títulos de páginas para obtener información
   de desambiguación.

   ::

      graphbot collect_confusables [<db>]


Generación de informes
----------------------

Una vez completado el análisis, pueden generarse páginas de informes pensadas
para ser subidas al proyecto:

.. code-block:: bash

    graphbot write [<db>]

Este comando generará:

- Páginas faltantes más enlazadas en el proyecto.
- Secciones de páginas faltantes más enlazadas en el proyecto.
- Secciones de páginas faltantes más enlazadas en el proyecto para un nombre de
  sección concreto. Se generarán informes para las 10 secciones más relevantes.


Para obtener el informe de secciones faltantes para una única sección en particular,
puede especificarse:

.. code-block:: bash

    graphbot write --language <nombre de sección> [<db>]


Subida de informes
~~~~~~~~~~~~~~~~~~

Los informes generados pueden subirse al sitio mediante ``pywikibot``:

.. code-block:: bash

    graphbot upload

Para llevar a cabo esta acción, el archivo ``user-config.py`` debe contar con
credenciales de usuario con permisos de escritura en el sitio.

Otros comandos
--------------

Para conocer el estado de la base de datos:

.. code-block:: bash

    graphbot info [<db>]

Para expandir texto en el contexto del sitio volcado:

.. code-block:: bash

    graphbot expand [-t <título>] <texto> [<db>]


A modo de ejemplo, expansión de nombre de página:

.. code-block:: bash

    $ echo "{{FULLPAGENAME}}" | graphbot expand -t "Template:hola" -
    Plantilla:hola

Para consultar la lista de páginas (existentes o no), similares en nombre
a una dada:

.. code-block:: bash

    graphbot disambiguation <título> [<db>]


A modo de ejemplo, páginas similares a "hola":

.. code-block:: bash

    $ graphbot disambiguation "hola"
    hola-
    holà
    holá
    hòla
    hóla

Páginas similares a "hola" en el ámbito de la sección "Español":

.. code-block:: bash

    $ graphbot disambiguation -l "Español" "hola"
    holá


Personalización
---------------

Para mejorar la expansión de ciertas páginas, se han reemplazado algunas
plantillas y módulos Lua en ``graphbot.bot.templates``.

Dependencias de software
------------------------

- `wikiexpand`_
- `mwparserfromhell`_
- `pywikibot`_
- `sqlalchemy`_
- `sqlite3`_
- `jinja2`_


.. _es.wiktionary.org: https://es.wiktionary.org
.. _pywikibot: https://www.mediawiki.org/wiki/Manual:Pywikibot
.. _user-config.py: https://www.mediawiki.org/wiki/Manual:Pywikibot/user-config.py
.. _dumps.wikimedia.org: https://dumps.wikimedia.org/backup-index.html
.. _pypy: https://pypy.org/
.. _sqlite3: https://www.sqlite.org/
.. _wikiexpand: https://bitbucket.org/wmj/wikiexpand
.. _mwparserfromhell: https://github.com/earwig/mwparserfromhell
.. _sqlalchemy: https://www.sqlalchemy.org/
.. _jinja2: http://jinja.pocoo.org/
