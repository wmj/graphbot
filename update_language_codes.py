#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

from graphbot.language_codes import main


if __name__ == "__main__":
    main()
