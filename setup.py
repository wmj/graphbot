#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from setuptools import find_packages, setup
import sys
import os
import codecs
import graphbot

IS_PY2 = sys.version_info.major == 2


def read(fname):
    try:
        with codecs.open(os.path.join(os.path.dirname(__file__), fname),
                         'r', 'utf-8') as f:
            return f.read()
    except:
        return ''


__author__ = "wmj"
__email__ = "wmj.py@gmx.com"
__version__ = graphbot.__version__
__description__ = graphbot.__doc__
__license__ = "LGPLv3"

requirements = [
    "wikiexpand==0.2.2",
    "sqlalchemy>=1.1",
    "jinja2",
    'functools32;python_version<"3"',
    #"pywikibot>=2.0rc3",
    #"mwparserfromhell>=0.4.3",
]


config = {
    'name': "graphbot",
    'version': __version__,
    'author': __author__,
    'author_email': __email__,
    'description': __description__,
    'license': __license__,
    'keywords': "mediawiki templates expansion",
    'url': "https://bitbucket.org/wmj/wikiexpand",
    'packages': find_packages(),
    'long_description': read("README.rst"),
    #'scripts': ["graphbot-cli"],
    'entry_points': {
        'console_scripts': [
            'graphbot=graphbot.bot.cmd:main',
            'graphbot-update-language-codes=graphbot.language_codes:main',
        ],
    },
    'install_requires': requirements,
    'dependency_links': [
        #"http://github.com/earwig/mwparserfromhell/tarball/develop#egg=mwparserfromhell",
        #"http://tools.wmflabs.org/pywikibot/core.tar.gz#egg=pywikibot-2.0rc5"
    ],
    #'include_package_data': True,
    'setup_requires': ['nose>=1.0'],
    'test_suite': 'nose.collector',
    'classifiers': [
        "Environment :: Console",
        "Topic :: Utilities",
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
    ]
}

setup(**config)
