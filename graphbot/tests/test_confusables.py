#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function

from graphbot import confusable as uc


def test_confusables1():

    c = uc.ConfusableHash()

    pairs = [
        ("amo", "amó"),
        ("Àmo", "amó"),
        ("veina", "veïna"),
        ("veinà", "veïna"),
        ("ōĭã", "oía"),
    ]

    for x, y in pairs:
        yield check_confusable_intersects, c, x, y


def check_confusable_intersects(hasher, a, b):

    set_a = frozenset(hasher.hash_strings(a))
    set_b = frozenset(hasher.hash_strings(b))

    assert not set_a.isdisjoint(set_b)
