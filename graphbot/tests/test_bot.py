#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function

from wikiexpand.wiki.xml_dump import XMLPage
from wikiexpand.expand.tools import p3_str
from graphbot.db.model import Model, Page, Section, Heading, SectionLink, Status
from graphbot.db.context import Progress
from graphbot.bot import GraphBot

from pprint import pprint
from datetime import datetime
import pywikibot as pw


PW_SITE = pw.Site("es", "wiktionary")


def _gen(pages):
    now = datetime.now()
    for p in pages:
        page = p.copy()
        page.update({
            "ns": 0,
            "timestamp": now,
            "revisionid": 0,
            "isredirect": False,
        })
        yield XMLPage(page)


#def load_data(pages):
#    db = Model.sqlite("")
#    gen = _gen(pages)
#
#    bot = GraphBot(db, gen)
#
#    for page in gen:
#        with db.session_scope() as session:
#            bot.process_page(session, page)
#
#    return db


def load_data(pages):
    db = Model.sqlite("")

    for page in _gen(pages):
        with db.session_scope() as session:
            p = Page.find(session, name=page.title)
            session.merge(p)

    with db.session_scope() as session:
        status = Status.singleton(session)
        status.lastop = Progress.CONTEXT
        status.size = len(list(pages))
        session.merge(status)

    bot = GraphBot(db, enumerate(_gen(pages)), pw_site=PW_SITE)

    bot.run()

    return db


P1 = {
    "title": "mé",
    "text": "[[mór#Irlandés]] [[mór#Euskera]] [[nada#Español]]"
}

P1b = {
    "title": "mé",
    "text": "[[mór#Euskera]] [[nada#Español]]"
}

P2 = {
    "title": "mór",
    "text": """
bla

== Español ==

* [[mór]]
* [[mór#Irlandés]]

== [[REF/Irlandés|Irlandés]] ==

* [[mór#Irlandés]]
* [[mór#Euskera]]

bla"""
}

P3 = {
    "title": "mór",
    "text": """<div style="align: right; padding:2px; border-bottom:0.1em dotted green; text-align:right; padding-right:25px; color:green">[[Imagen:Disambig.svg|25px|icono de desambiguación|link=]]&nbsp;''Entradas similares:''&nbsp; '''[[mor]]''', '''[[mòr]]''', '''[[môr]]'''[[Categoría:Wikcionario:Desambiguación]]</div>

== <span id="gd" class="headline-lang">Gaélico escocés</span>[[Categoría:Gaélico escocés-Español]] ==
{|class="toccolours" cellspacing="0.1" style="float: right; clear: right; margin: 0.2em 0 0.2em 1.4em;"
|style="background:#EBEBEB; text-align:left; font-weight:420" colspan="2"|&nbsp;<font size=4><font face=helvetica>mór</font></font>

|-
|style="vertical-align:top"|Pronunciación&nbsp;([[Alfabeto Fonético Internacional|AFI]]):&nbsp;
|<span style="color: green; font-size: 90%; font-style: italic;">Si puedes, [[Wikcionario:Pronunciación|¡incorpórala!]]</span>[[Categoría:Wikcionario:GD:Palabras sin transcripción fonética]]
|-

|}

=== Etimología ===
<span class="etym-transition-label">'''Etimología:'''&nbsp;</span><span class="etym-transition-old">del</span><span class="etym-transition-new">Del</span> irlandés antiguo <span lang="sga" xml:lang="sga" style="font-style:italic;" class="">[[mór#Irlandés antiguo|mór]]</span>[[Categoría:GD:Palabras de origen irlandés antiguo]]

=== Adjetivo[[Categoría:GD:Adjetivos]] ===
;1: <span class="definicion-impropia" style="font-style: italic;">Grafía alternativa de&nbsp;</span><span lang="gd" xml:lang="gd" style="" class="">[[mòr#Gaélico escocés|mòr]]</span>[[Categoría:GD:Grafías alternativas]]
:*'''Uso:''' Obsoleto[[Categoría:GD:Términos obsoletos]]


== <span id="ga" class="headline-lang">[[Wikcionario:Referencia/GA|Irlandés]]</span>[[Categoría:Irlandés-Español]] ==
{|class="toccolours" cellspacing="0.1" style="float: right; clear: right; margin: 0.2em 0 0.2em 1.4em;"
|style="background:#EBEBEB; text-align:left; font-weight:420" colspan="2"|&nbsp;<font size=4><font face=helvetica>mór</font></font>

|-
|style="vertical-align:top"|Pronunciación&nbsp;([[Alfabeto Fonético Internacional|AFI]]):&nbsp;
|[<span style="color:#368BC1">moːr</span style>]
|-


|}

=== Etimología ===
<span class="etym-transition-label">'''Etimología:'''&nbsp;</span><span class="etym-transition-old">del</span><span class="etym-transition-new">Del</span> irlandés antiguo <span lang="sga" xml:lang="sga" style="font-style:italic;" class="">[[mór#Irlandés antiguo|mór]]</span>[[Categoría:GA:Palabras de origen irlandés antiguo]]

=== Adjetivo[[Categoría:GA:Adjetivos]] ===
[[Categoría:GA:Lista Swadesh|027mór]]
[[Categoría:GA:Adjetivos prefijos]]
{| class="inflection-table"
! class="vertical" |Base
! class="vertical" |Suave
! class="vertical" |Nasal
|-
| mór
| <span lang="ga" xml:lang="ga" style="" class="">[[mhór#Irlandés|mhór]]</span>
| mór
|}
{| class="inflection-table collapsible autocollapse" style="float: right;"
|+ class="normal" | <span lang="ga" xml:lang="ga"><big>'''mór'''</big></span><br/>''primera declinación''
|-
! rowspan="2" | <small>Positivo</small>
! colspan=2 style="text-align: center;" |Singular
! colspan=2 style="text-align: center;" |Plural
|-
! style="text-align: center;" | masc.
! style="text-align: center;" | fem.
! style="text-align: center;" | fuerte
! style="text-align: center;" | débil
|-
! Nominativo
| colspan=2 style="text-align: center;"| <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[mór#Irlandés|mór]]</span>
</span>
| colspan=2 style="text-align: center;" | <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[móra#Irlandés|móra]]</span></span>
|-
! Vocativo
|| <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[móir#Irlandés|móir]]</span>
</span>
|| <span lang="ga" xml:lang="ga">mór
</span>
| colspan=2 style="text-align: center;" | <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[móra#Irlandés|móra]]</span></span>
|-
! Genitivo
|| <span lang="ga" xml:lang="ga">móir
</span>
|| <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[móire#Irlandés|móire]]</span>
</span>
|| <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[móra#Irlandés|móra]]</span>
</span>
|| <span lang="ga" xml:lang="ga">mór</span>
|-
! Dativo
| colspan=2 style="text-align: center;" | <span lang="ga" xml:lang="ga">mór
</span>
| colspan=2 style="text-align: center;" | <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[móra#Irlandés|móra]]</span></span>
|-
! <small>Comparativo<br/>Superlativo</small>
| colspan=4 style="text-align: center;" | <span lang="ga" xml:lang="ga"><span lang="ga" xml:lang="ga" style="" class="">[[mó#Irlandés|mó]]</span></span>

|}

;1: [[grande#Español|Grande]]
:*'''Ejemplos:'''
::"<span lang="ga" xml:lang="ga" style="" class="">[[d'éirigh#Irlandés|Dh'éirigh]]</span> <span lang="ga" xml:lang="ga" style="" class="">[[a#Irlandés|a]]</span> <span lang="ga" xml:lang="ga" style="" class="">[[bhean#Irlandés|bhean]]</span>, <span lang="ga" xml:lang="ga" style="" class="">[[agus#Irlandés|agus]]</span> <span lang="ga" xml:lang="ga" style="" class="">[[bhí#Irlandés|bhí]]</span> <span lang="ga" xml:lang="ga" style="" class="">[[meidhir#Irlandés|meidhir]]</span> <span lang="ga" xml:lang="ga" style="" class="">[[mhór#Irlandés|mhór]]</span> agus <span lang="ga" xml:lang="ga" style="" class="">[[áthas#Irlandés|áthas]]</span> uirthi <span lang="ga" xml:lang="ga" style="" class="">[[é#Irlandés|é]]</span> a <span lang="ga" xml:lang="ga" style="" class="">[[d'fheiscint#Irlandés|dh'fheiscint]]</span> <span lang="ga" xml:lang="ga" style="" class="">[[arís#Irlandés|arís]]</span>, agus <span lang="ga" xml:lang="ga" style="" class="">[[dhein#Irlandés|dhein]]</span> sí <span lang="ga" xml:lang="ga" style="" class="">[[suipéar#Irlandés|suipéar]]</span>." <cite style="font-style:normal" > <i>[http://wikisource.org/wiki/Sc%C3%A9al_na_dtr%C3%AD_chomhairle Scéal na dtrí chomhairle] </i>.</cite>

== <span id="sga" class="headline-lang">Irlandés antiguo</span>[[Categoría:Irlandés antiguo-Español]] ==
{|class="toccolours" cellspacing="0.1" style="float: right; clear: right; margin: 0.2em 0 0.2em 1.4em;"
|style="background:#EBEBEB; text-align:left; font-weight:420" colspan="2"|&nbsp;<font size=4><font face=helvetica>mór</font></font>

|-
|style="vertical-align:top"|Pronunciación&nbsp;([[Alfabeto Fonético Internacional|AFI]]):&nbsp;
|<span style="color: green; font-size: 90%; font-style: italic;">Si puedes, [[Wikcionario:Pronunciación|¡incorpórala!]]</span>[[Categoría:Wikcionario:SGA:Palabras sin transcripción fonética]]
|-

|-
| scope="row" style="vertical-align:top" | Variante:&nbsp;
|<span lang="sga" xml:lang="sga" style="" class="">[[már#Irlandés antiguo|már]]</span>
|-

|}

=== Etimología ===
<span class="etym-transition-label">'''Etimología:'''&nbsp;</span><span style="color:green; font-size:90%">''<span class="etym-transition-old">← si</span><span class="etym-transition-new">Si</span> puedes, incorpórala: [[Plantilla:etimología| ver cómo]]''</span>[[Categoría:Wikcionario:SGA:Palabras de etimología sin precisar]]

=== Adjetivo[[Categoría:SGA:Adjetivos]] ===
[[Categoría:SGA:Lista Swadesh|027mór]]

;1: [[grande#Español|Grande]]

<div style="clear: both;"></div>

=== Información adicional ===
<div class="NavFrame collapsed" style="clear:both">
<div class="NavHead" align=center>Derivados de «mór» en otras lenguas:</div>
<div class="NavContent">
{| border="0" width="100%" style="width:100%; text-align: left;"
|-
| valign="top" width="50%"|
* Irlandés:   <span lang="ga" xml:lang="ga" style="" class="">[[mór#Irlandés|mór]]</span> <sup>[[:ga:mór|(ga)]]</sup>
* Gaélico escocés:   <span lang="gd" xml:lang="gd" style="" class="">[[mòr#Gaélico escocés|mòr]]</span> <sup>[[:gd:mòr|(gd)]]</sup>
|valign="top" width="50%"|
* Manés:   <span lang="gv" xml:lang="gv" style="" class="">[[mooar#Manés|mooar]]</span> <sup>[[:gv:mooar|(gv)]]</sup>
|}
</center>

</h1>

</div>

</div>

== Referencias y notas ==
<references />

"""
}


def check_section_blue(pages):
    db = load_data(pages)

    with db.session_scope() as session:
        qh = session.query(Heading)
        h_esp = qh.filter_by(name="Español").one()
        h_irl = qh.filter_by(name="Irlandés").one()
        h_eus = qh.filter_by(name="Euskera").one()

        p1 = session.query(Page).filter_by(name="mór").one()

        qs = session.query(Section)

        assert len(p1.sections) == 3

        section = qs.filter_by(page=p1, heading=h_esp).one()

        assert section.blue

        section = qs.filter_by(page=p1, heading=h_irl).one()

        assert section.blue

        section = qs.filter_by(page=p1, heading=h_eus).one()

        assert not section.blue


def check_section_blue2(pages):
    db = load_data(pages)

    with db.session_scope() as session:
        p1 = session.query(Page).filter_by(name="mór").one()
        qs = session.query(Section)
        for s in qs.filter_by(page=p1):
            print(p3_str(s).encode("utf-8"), p3_str(s.heading).encode("utf-8"))
        qh = session.query(Heading)
        sections = [
            "Gaélico escocés",
            "Etimología",
            "Adjetivo",
            "Irlandés",
            "Etimología 2",
            "Adjetivo 2",
            "Irlandés antiguo",
            "Etimología 3",
            "Adjetivo 3",
            "Información adicional"
        ]

        headings = []
        for s in sections:
            print("section", s)
            h = qh.filter_by(name=s).one()
            headings.append(h)

        for h in headings:
            section = qs.filter_by(page=p1, heading=h).one()
            assert section.blue


def test_section_blue_pre_insert():
    pages = [P1, P2]

    yield check_section_blue, pages


def test_section_blue_post_insert():
    pages = [P2, P1]

    yield check_section_blue, pages


def test_section_blue_post_insert_b():
    pages = [P2, P1b]

    yield check_section_blue, pages


def test_section_blue_pre_insert_b():
    pages = [P1b, P2]

    yield check_section_blue, pages


def test_section_blue2():
    pages = [P3]

    yield check_section_blue2, pages


P_SIM = {"title": "sim",
         "text": """

<br clear="all"/>
=<div id="la" class="toccolours" >'''Latín'''</div>=

<div class="lemma">sim</div>[[Categoría:Latín-Español]]

*'''sim:'''&nbsp;[&nbsp;sĩː&nbsp;] <small>([[Wikcionario:Referencia/LA/Pronunciación|latín clásico]], [[Alfabeto Fonético Internacional|AFI]])</small>

===Forma verbal===
;1: SUBST.


<br clear="all"/>
=<div id="pt" class="toccolours" >'''Portugués'''</div>=

<div class="lemma">sim</div>[[Categoría:Portugués-Español]]

*<span class="pron-transition-old">'''Pronunciación:'''</span><span class="pron-transition-new">'''sim:'''</span>&nbsp; <span class="IPA">[ sĩ ]</span>&nbsp;<small>([[Alfabeto Fonético Internacional|AFI]])</small>
<span class="etym-transition-label">'''Etimología:'''&nbsp;</span><span class="etym-transition-old">del</span><span class="etym-transition-new">Del</span> galaicoportugués <span lang="roa-ptg" xml:lang="roa-ptg" style="font-style:italic;" class="">[[si#Galaicoportugués|si]]</span>[[Categoría:PT:Palabras de origen galaicoportugués]], y este del latín <span lang="la" xml:lang="la" style="font-style:italic;" class="">[[sic#Latín|sīc]]</span>&nbsp;("[[así]]"), del protoindoeuropeo <span lang="ine" xml:lang="ine" style="font-style:italic;" class="">[[*so#Protoindoeuropeo|*so]]</span>. Compárese el arrumano <span lang="roa-rup" xml:lang="roa-rup" style="font-style:italic;" class="">[[shi#|shi]]</span>, el asturiano <span lang="ast" xml:lang="ast" style="font-style:italic;" class="">[[sí#Asturiano|sí]]</span>, el castellano <span lang="es" xml:lang="es" style="font-style:italic;" class="">[[sí#Español|sí]]</span>, el catalán <span lang="ca" xml:lang="ca" style="font-style:italic;" class="">[[sí#Catalán|sí]]</span>, el corso <span lang="co" xml:lang="co" style="font-style:italic;" class="">[[sì#Corso|sì]]</span>, el francés <span lang="fr" xml:lang="fr" style="font-style:italic;" class="">[[si#Francés|si]]</span>, el friulano <span lang="fur" xml:lang="fur" style="font-style:italic;" class="">[[sì#Friulano|sì]]</span>, el galiciano <span lang="gl" xml:lang="gl" style="font-style:italic;" class="">[[si#Gallego|si]]</span>, el italiano <span lang="it" xml:lang="it" style="font-style:italic;" class="">[[sì#Italiano|sì]]</span>, el romanche <span lang="rm" xml:lang="rm" style="font-style:italic;" class="">[[schi#Romanche|schi]]</span>, el rumano <span lang="ro" xml:lang="ro" style="font-style:italic;" class="">[[și#Rumano|și]]</span>, el siciliano <span lang="scn" xml:lang="scn" style="font-style:italic;" class="">[[sè#Siciliano|sè]]</span>. Nota que el portugués <span lang="pt" xml:lang="pt" style="font-style:italic;" class="">[[sic#Portugués|sic]]</span> es un derivado directo.
*'''Grafía alternativa:'''&nbsp;<span lang="pt" xml:lang="pt" style="" class="">[[si#Portugués|si]]</span>&nbsp;(obsoleta)[[Categoría:PT:Palabras con varias grafías]].

=== Adverbio[[Categoría:PT:Adverbios]] ===

;1: [[afirmativamente#Español|Afirmativamente]], [[de verás]], [[de verdad]], [[ciertamente]] o [[sí]].
;2: [[así#Español|Así]].
:*'''Uso:''' obsoleto[[Categoría:PT:Términos obsoletos]].
:*'''Sinónimo:''' <span lang="pt" xml:lang="pt" style="" class="">[[assim#Portugués|assim]]</span>.

=== Interjección[[Categoría:PT:Interjecciones]] ===

;3: [[absolutamente#Español|Absolutamente]], [[afirmativo]], [[bien]], [[correcto]], [[de acuerdo]] o [[sí]].
:*'''Sinónimos:''' <span lang="pt" xml:lang="pt" style="" class="">[[absolutamente#Portugués|absolutamente]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[afirmativo#Portugués|afirmativo]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[bem#Portugués|bem]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[correcto#Portugués|correcto]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[é#Portugués|é]]</span>.
:*'''Antónimos:''' <span lang="pt" xml:lang="pt" style="" class="">[[não#Portugués|não]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[negativo#Portugués|negativo]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[nop#Portugués|nop]]</span>.

=== Sustantivo&nbsp;masculino[[Categoría:PT:Sustantivos]][[Categoría:PT:Sustantivos&nbsp;masculinos]] ===
{|class="inflection-table"

! class="vertical" style="padding-left:4px; padding-right:4px" width=50% | Singular
! class="vertical" style="padding-left:4px; padding-right:4px" width=50% | Plural
|-

|lang="pt" xml:lang="pt" dir="ltr" style="text-align:center;padding-left:4px; padding-right:4px" | sim
|lang="pt" xml:lang="pt" dir="ltr" style="text-align:center;padding-left:4px; padding-right:4px" | sins

|}

;4: [[sí#Español|Sí]].

==Locuciones==
<div class="NavFrame autocollapse" style="clear:both">
<div class="NavHead" align=center>Términos relacionados</div>
<div class="NavContent">
{| border="0" width="100%" class="related terms"
|-
| bgcolor="#F8F8FF" valign="top" width="48%" align=left |
* <span lang="pt" xml:lang="pt" style="" class="">[[dar o sim#Portugués|dar o sim]]</span>
* <span lang="pt" xml:lang="pt" style="" class="">[[isso sim#Portugués|isso sim]]</span>
| width=1% |
|bgcolor="#F8F8FF" valign=top align=left width=48%|
* <span lang="pt" xml:lang="pt" style="" class="">[[pelo sim pelo não#Portugués|pelo sim pelo não]]</span>
* <span lang="pt" xml:lang="pt" style="" class="">[[pois sim#Portugués|pois sim]]</span>
* <span lang="pt" xml:lang="pt" style="" class="">[[porque sim#Portugués|porque sim]]</span>
|}
</div></div>

== Información adicional ==
*'''Derivados o cognados:''' <span lang="pt" xml:lang="pt" style="" class="">[[assim#Portugués|assim]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[outrossim#Portugués|outrossim]]</span>, <span lang="pt" xml:lang="pt" style="" class="">[[sic#Portugués|sic]]</span>.


<br clear="all"/>
=<div id="toc" class="toccolours" >'''Transliteración&nbsp;del amárico[[Categoría:Transliteraciones del amárico]]'''</div>=
<div class="lemma">sim</div>
:*'''Grafías alternativas:''' Abugida amárico: [[ፂም]]

===Sustantivo[[Categoría:AM:Sustantivos]]===

;1:[[barba|Barba]].

[[sima#ZZZ]]
[[sima#Latín]]

= <small>Referencias y notas</small> =
<references />"""}


P4 = {
    "title": "sima",
    "text": """
== Latín ==

[[ፂም#Amárico]]

== Portugués ==

[[sim#Inexistente]]
[[sim#Latín]]

== Swahili ==

[[não#Portugués]]
"""
}


def check_section_blue_1(db, pagename, section):

    with db.session_scope() as session:
        page = session.query(Page).filter_by(name=pagename).one()
        qh = session.query(Heading)
        qs = session.query(Section)

        h = qh.filter_by(name=section).one_or_none()

        assert h is not None, "heading <%s> exists" % section

        db_section = qs.filter_by(page=page, heading=h).one_or_none()

        assert db_section is not None, "section <%s, %s> exists" % (pagename, section)

        assert db_section.blue, "section <%s> is blue" % db_section


def test_section_sim_blue():
    pages = [P_SIM, P4]

    sections = {
        "sim": [
            "Latín",
            "Forma verbal",
            "Portugués",
            "Adverbio",
            "Interjección",
            "Sustantivo masculino",
            "Locuciones",
            "Transliteración del amárico",
            "Sustantivo",
            "Información adicional",
            "Referencias y notas"
        ],
        "sima": [
            "Latín",
            "Portugués",
            "Swahili",
        ],
    }

    db = load_data(pages)

    for pagename, page_sections in sections.items():
        for s in page_sections:
            yield check_section_blue_1, db, pagename, s

    db2 = load_data(list(reversed(pages)))

    for pagename, page_sections in sections.items():
        for s in page_sections:
            yield check_section_blue_1, db2, pagename, s


def check_heading(db, heading):

    with db.session_scope() as session:
        assert session.query(Heading).filter_by(name=heading).limit(1).count()


def test_headings_from_links():
    pages = [P_SIM, P4]

    headings = [
        'Asturiano',
        'Catalán',
        'Corso',
        'Español',
        'Francés',
        'Friulano',
        'Galaicoportugués',
        'Gallego',
        'Italiano',
        'Latín',
        'Portugués',
        'Protoindoeuropeo',
        'Romanche',
        'Rumano',
        'Siciliano',
        'ZZZ',

        'Amárico',
        'Inexistente',
        'Swahili',
    ]

    db = load_data(pages)

    for h in headings:
        yield check_heading, db, h

    db2 = load_data(list(reversed(pages)))

    for h in headings:
        yield check_heading, db2, h


def check_link(db, pagename, link):

    with db.session_scope() as session:
        p1 = session.query(Page).filter_by(name=pagename).one_or_none()
        assert p1
        links = [x.target.name for x in p1.links]
        assert link in links


def test_links():
    pages = [P_SIM, P4]

    links = {
        "sim": [
            '*so',
            'Alfabeto Fonético Internacional',
            'absolutamente',
            'afirmativamente',
            'afirmativo',
            'assim',
            'así',
            'barba',
            'bem',
            'bien',
            'ciertamente',
            'correcto',
            'dar o sim',
            'de acuerdo',
            'de verdad',
            'de verás',
            'isso sim',
            'negativo',
            'nop',
            'não',
            'outrossim',
            'pelo sim pelo não',
            'pois sim',
            'porque sim',
            'schi',
            'shi',
            'si',
            'sic',
            'sè',
            'sì',
            'sí',
            'é',
            'și',
            'ፂም',
            'sima',
        ],
        "sima": [
            'ፂም',
            'sim',
            'não',
        ]
    }

    db = load_data(pages)

    for pagename, page_links in links.items():
        for l in page_links:
            yield check_link, db, pagename, l

    db2 = load_data(list(reversed(pages)))

    for pagename, page_links in links.items():
        for l in page_links:
            yield check_link, db2, pagename, l


def check_section_link(db, pagename, link, section):

    with db.session_scope() as session:
        p1 = session.query(Page).filter_by(name=pagename).one_or_none()
        assert p1
        q = session.query(Section)\
            .join(SectionLink.section)\
            .filter(SectionLink.page == p1)
        all_sections = q.all()

        all_sections_names = [x.page.name for x in all_sections]
        print("SECTIONS LINKED FROM '%s' TO" % pagename)
        pprint(all_sections_names)

        assert link in all_sections_names, "link not found"

        link_sections = [x.heading.name
                         for x in all_sections if x.page.name == link]
        print("SECTIONS OF '%s'" % link)
        pprint(link_sections)

        assert section in link_sections, "section link not found"


def test_section_links():
    pages = [P_SIM, P4]

    section_links = {
        "sim": [
            ('*so', 'Protoindoeuropeo'),
            ('absolutamente', 'Español'),
            ('absolutamente', 'Portugués'),
            ('afirmativamente', 'Español'),
            ('afirmativo', 'Portugués'),
            ('assim', 'Portugués'),
            ('así', 'Español'),
            ('bem', 'Portugués'),
            ('correcto', 'Portugués'),
            ('dar o sim', 'Portugués'),
            ('isso sim', 'Portugués'),
            ('negativo', 'Portugués'),
            ('nop', 'Portugués'),
            ('não', 'Portugués'),
            ('outrossim', 'Portugués'),
            ('pelo sim pelo não', 'Portugués'),
            ('pois sim', 'Portugués'),
            ('porque sim', 'Portugués'),
            ('schi', 'Romanche'),
            ('si', 'Francés'),
            ('si', 'Galaicoportugués'),
            ('si', 'Gallego'),
            ('si', 'Portugués'),
            ('sic', 'Latín'),
            ('sic', 'Portugués'),
            ('sè', 'Siciliano'),
            ('sì', 'Corso'),
            ('sì', 'Friulano'),
            ('sì', 'Italiano'),
            ('sí', 'Asturiano'),
            ('sí', 'Catalán'),
            ('sí', 'Español'),
            ('é', 'Portugués'),
            ('și', 'Rumano'),
            ('sima', 'Latín'),
            ('sima', 'ZZZ'),
        ],
        "sima": [
            ("ፂም", "Amárico"),
            ("sim", "Inexistente"),
            ("sim", "Latín"),
            ("não", "Portugués"),
        ],
    }

    db = load_data(pages)

    for pagename, page_section_links in section_links.items():
        for target, section in page_section_links:
            yield check_section_link, db, pagename, target, section

    db2 = load_data(list(reversed(pages)))

    for pagename, page_section_links in section_links.items():
        for target, section in page_section_links:
            yield check_section_link, db2, pagename, target, section
