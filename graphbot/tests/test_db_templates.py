#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import unicode_literals
from __future__ import print_function

#from wikiexpand.db.model import Model
from graphbot.db.templates import TemplateDB


#def test_redirect():
#    T = TemplateDB.connect(":memory:")
#    T.clear()
#    T.commit()
#
#    text = "unus una uno"
#    T["uno"] = text
#
#    T.commit()
#
#    assert(T["uno"] == text)
#
#    T.redirect("tres", "dos")
#    T.redirect("dos", "uno")
#
#    T.commit()
#
#    assert(T["uno"] == text)
#    assert(T["dos"] == text)
#    assert(T["tres"] == text)
#
#    T.connection.close()

def test_redirect2():
    from graphbot.db.model import Model

    db = Model.sqlite("")

    text = "unus una uno"
    text2 = "duo duae duo"

    with db.session_scope() as session:
        T = TemplateDB(session)

        T.clear()

        T["uno"] = text

        assert(T["uno"] == text)

        T.redirect("tres", "dos")
        T.redirect("dos", "uno")

        assert(T["uno"] == text)
        assert(T["dos"] == text)
        assert(T["tres"] == text)

        T["uno"] = text2

        assert(T["uno"] == text2)
        assert(T["dos"] == text2)
        assert(T["tres"] == text2)
