#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import unicode_literals
from __future__ import print_function

from graphbot.db.model import (
    Model,
    Page,
    Heading,
    Section,
    SectionLink
)

from nose.tools import assert_list_equal

import itertools
from pprint import pprint


class TestBulkMethods(object):

    def setUp(self):
        self.db = Model.sqlite("")
        self.names = ["page%02d" % x for x in range(20)]

    def test_insert_pages(self):

        with self.db.session_scope() as session:

            # insert A
            ret = Page.insert_or_ignore(session, self.names[:4])

            assert len(ret) == len(self.names[:4])

            session.flush()

            out = [x for x, in session.query(Page.name).order_by(Page.name).all()]

            assert_list_equal(self.names[:4], out)

            # insert B
            ret = Page.insert_or_ignore(session, self.names[2:8])

            assert len(ret) == len(self.names[2:8])

            session.flush()

            out = [x for x, in session.query(Page.name).order_by(Page.name).all()]

            assert_list_equal(self.names[:8], out)

    def test_insert_headings(self):

        with self.db.session_scope() as session:

            # insert A
            ret = Heading.insert_or_ignore(session, self.names[:4])

            assert len(ret) == len(self.names[:4])

            session.flush()

            out = [x for x, in session.query(Heading.name).order_by(Heading.name).all()]

            assert_list_equal(self.names[:4], out)

            # insert B
            ret = Heading.insert_or_ignore(session, self.names[2:8])

            assert len(ret) == len(self.names[2:8])

            session.flush()

            out = [x for x, in session.query(Heading.name).order_by(Heading.name).all()]

            assert_list_equal(self.names[:8], out)

    def test_insert_links(self):

        with self.db.session_scope() as session:

            Page.insert_or_ignore(session, self.names)

            page = Page.find(session, name="main")

            session.flush()

            # insert links A

            page.insert_or_ignore_links(session, self.names[:4])

            out = sorted(x.target.name for x in page.links)

            assert_list_equal(self.names[:4], out)

            # insert links B

            page.insert_or_ignore_links(session, self.names[2:8])

            session.flush()

            session.refresh(page)

            out = sorted(x.target.name for x in page.links)

            assert_list_equal(self.names[:8], out)

    def test_insert_sections(self):

        with self.db.session_scope() as session:

            # insert pages
            p_ids = Page.insert_or_ignore(session, self.names[:4])

            # insert headings
            h_ids = Heading.insert_or_ignore(session, self.names[:4])

            ref = sorted(list(itertools.product(p_ids, h_ids)))

            print("ref")
            pprint(ref)

            session.flush()

            rows = [{"page_id": p_ids[x], "heading_id": h_ids[y]} for x, y in ref[::2]]
            tuples = [(x["page_id"], x["heading_id"]) for x in rows]

            print("len tuples", len(tuples))

            print("tuples")
            pprint(tuples)

            Section.insert_or_ignore(session, rows)

            session.flush()

            out = session.query(Section.page_id, Section.heading_id).all()
            out = list(out)

            print("out")
            pprint(out)

            assert_list_equal(tuples, out)

            rows = [{"page_id": p_ids[x], "heading_id": h_ids[y]} for x, y in ref]
            tuples = [(x["page_id"], x["heading_id"]) for x in rows]

            print("len tuples", len(tuples))

            print("tuples")
            pprint(tuples)

            Section.insert_or_ignore(session, rows)

            session.flush()

            out = session.query(Section.page_id, Section.heading_id).all()
            out = sorted(out)

            print("out")
            pprint(out)

            assert_list_equal(sorted(tuples), sorted(out))

    def test_insert_section_links(self):

        with self.db.session_scope() as session:

            # insert pages
            p_ids = Page.insert_or_ignore(session, self.names[:4])

            # insert headings
            h_ids = Heading.insert_or_ignore(session, self.names[:4])

            session.flush()

            ref = sorted(list(itertools.product(p_ids, h_ids)))

            ref_id = sorted([(p_ids[x], h_ids[y]) for x, y in ref])
            rows = [{"page_id": x, "heading_id": y} for x, y in ref_id]

            Section.insert_or_ignore(session, rows)

            session.flush()

            # insert section links
            page = Page.find(session, name="main")

            session.flush()

            # insert half of the section links

            page.insert_or_ignore_section_links(session, ref[::2])

            session.flush()

            q1 = session.query(SectionLink.section_id)\
                .filter_by(page=page)

            q = session.query(Section.page_id, Section.heading_id)\
                .filter(Section.id.in_(q1))

            out = q.all()

            assert_list_equal(ref_id[::2], sorted(out))

            # insert all the section links

            page.insert_or_ignore_section_links(session, ref)

            session.flush()

            out = q.all()

            assert_list_equal(ref_id, sorted(out))

    def test_set_existing_sections(self):

        with self.db.session_scope() as session:

            # insert headings
            h_ids = Heading.insert_or_ignore(session, self.names)

            # insert section links
            page = Page.find(session, name="main")

            session.flush()

            rows = [{"page_id": page.id, "heading_id": v} for v in h_ids.values()]

            Section.insert_or_ignore(session, rows)

            session.flush()

            blue_sections = self.names[::2]

            page.set_existing_sections(session, blue_sections)

            session.flush()

            session.refresh(page)

            out = [(s.heading.name, s.blue) for s in page.sections]

            assert all(blue for heading, blue in out
                       if heading in blue_sections)

            assert not any(blue for heading, blue in out
                           if heading not in blue_sections)
