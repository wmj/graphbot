#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function

from graphbot import language_codes as codes


PAGE1 = """
[[Categoría:Wikcionario:Apéndices|idiomas]][[Categoría:Wikcionario:Ayuda]]
{{encabezado proyecto|izq=&larr; [[Ayuda:Mapa de ayuda|Mapa de ayuda]]|tít=Lista de códigos de idioma usados en Wikcionario|der=[[Wikcionario:Estructura]] &rarr;|atajo=WN:CI|notas=Esta lista se actualiza automáticamente según los idiomas de las entradas de ''Wikcionario'' y te pedimos que <u>no la edites manualmente</u> ([[User talk:Peter Bowman|notifica los errores aquí]]).<br>El '''[[Apéndice:Idiomas]]''' contiene una lista similar, editable, con información adicional sobre diversas lenguas.<br>Las plantillas correspondientes están ubicadas en '''[[:Categoría:Plantillas de idiomas]]'''.<br>Última actualización del bot: <span class="update-timestamp">03:14 20 ene 2018 (UTC) (1127 idiomas)</span>.}}
<!-- por favor, no edites esta línea ni las siguientes -->
{| class="wikitable sortable"
! Código !! Idioma
|-
| 0hk || darkinyung
|-
| 0hl || dharug
|-
| aa || afar
|-
| aan || anambé
|-
| aap || arará
|-
| ab || abjaso
|-
| abq || abaza
|-
| abs || malayo ambonés
|-
| abx || inabacnum
|-
| ace || achenés
|-
| ach || acholi
|-
| acv || achumawi
|}
"""


REF1 = frozenset([
    ("0hk" , "darkinyung"),
    ("0hl" , "dharug"),
    ("aa"  , "afar"),
    ("aan" , "anambé"),
    ("aap" , "arará"),
    ("ab"  , "abjaso"),
    ("abq" , "abaza"),
    ("abs" , "malayo ambonés"),
    ("abx" , "inabacnum"),
    ("ace" , "achenés"),
    ("ach" , "acholi"),
    ("acv" , "achumawi"),
])


def check_parse_code_table(page, ref):
    items = frozenset(codes.parse_code_table(page))
    assert ref == items


def test_parse_code_table():
    pairs = [
        (PAGE1, REF1),
    ]

    for page, ref in pairs:
        yield check_parse_code_table, page, ref



