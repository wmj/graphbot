#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import codecs
import os
import sys
import glob
import pywikibot as pw
import difflib
import pydoc
import jinja2

from wikiexpand.compat import p3_input
from ..db.model import Model, Status, Page, Heading, Section, LanguageCode
from ..db.context import Progress, UnexpectedLastOp, PreconditionException


APPENDIX = "Apéndice"
FILE_PREFIX = "%s:Secciones de páginas requeridas" % APPENDIX
EXTENSION = ".mediawiki"

FOLDER = "pages"
COMMENT = "secciones más enlazadas que no se han creado"


def _confusables(session, page_id):
    return [x[0] for x in Page.q_confusables(session, page_id)]


def _sec_confusables(session, page_id, h_id):
    return [x[0] for x in Page.q_section_confusables(session, page_id, h_id)]


class _Writer(object):

    def __init__(self,
                 folder=FOLDER,
                 max_items=500):
        self.folder = folder

        self.max_items = max_items
        self.dump_file = None

        if not os.path.exists(folder):
            os.makedirs(folder)

        # jinja templates
        self.env = jinja2.Environment(
            trim_blocks=True,
            loader=jinja2.PackageLoader(__package__, "jinja"))

    def _wanted_sections(self, session, filename, languages):
        tpl = self.env.get_template("es_wikt_wanted_sections.mediawiki")
        path = os.path.join(self.folder, filename)
        #print("Writing file '%s' ... " % path, file=sys.stderr)
        sys.stderr.write("Writing file '%s' ... " % path)
        sys.stderr.flush()
        with codecs.open(path, "w", encoding="utf-8") as fd:
            cov = Section.coverage(session)
            cov = (cov[0], cov[1], 100 * cov[0] / cov[1])
            query = Model.q_wanted_sections(session).limit(self.max_items)

            q_tuples = (
                (
                    oc,
                    title,
                    section,
                    _confusables(session, page_id),
                    _sec_confusables(session, page_id, heading_id)
                ) for oc, title, section, page_id, heading_id in query
            )

            for txt in tpl.generate(
                dump_file=self.dump_file,
                languages=(x[1:] for x in languages),
                query=q_tuples,
                coverage=cov
            ):
                fd.write(txt)

        print("done.", file=sys.stderr)

        return filename

    def _wanted_lang_sections(self, session, filename, lang_tuple):
        tpl = self.env.get_template(
            "es_wikt_wanted_sections_by_language.mediawiki")
        path = os.path.join(self.folder, filename)
        #print("Writing file '%s' ... " % path, file=sys.stderr)
        sys.stderr.write("Writing file '%s' ... " % path)
        sys.stderr.flush()
        with codecs.open(path, "w", encoding="utf-8") as fd:
            heading_id, heading, cnt, code = lang_tuple
            cov = Section.coverage(session, heading_id=heading_id)
            cov = (cov[0], cov[1], 100 * cov[0] / cov[1])

            annotated = Heading.q_annotated_headings(session, heading).all()
            query = Model.q_wanted_sections(session, heading)\
                .limit(self.max_items)

            q_tuples = (
                (
                    oc,
                    title,
                    section,
                    _confusables(session, page_id),
                    _sec_confusables(session, page_id, heading_id)
                ) for oc, title, section, page_id, heading_id in query
            )

            for txt in tpl.generate(
                dump_file=self.dump_file,
                heading=heading,
                total=cnt,
                annotated=annotated,
                query=q_tuples,
                coverage=cov,
                lang_code=code
            ):
                fd.write(txt)

        print("done.", file=sys.stderr)

        return filename

    def _wanted_pages(self, session, filename):
        tpl = self.env.get_template("es_wikt_wanted_pages.mediawiki")
        path = os.path.join(self.folder, filename)
        #print("Writing file '%s' ... " % path, file=sys.stderr)
        sys.stderr.write("Writing file '%s' ... " % path)
        sys.stderr.flush()
        with codecs.open(path, "w", encoding="utf-8") as fd:
            cov = Page.coverage(session)
            cov = (cov[0], cov[1], 100 * cov[0] / cov[1])
            pages = Model.q_wanted_pages(session).limit(self.max_items)

            def q_sections(page_id):
                s_by_l = Section.query_by_links(session, page_id).subquery()
                s_by_p = session.query(s_by_l, Heading.name.label("heading_name"))\
                    .join(Heading).subquery()
                sq = session.query(s_by_p.c.occurrences, s_by_p.c.heading_name)
                return sq.order_by(
                    s_by_p.c.occurrences.desc(),
                    s_by_p.c.heading_name
                )

            page_gen = (
                (
                    cnt,
                    title,
                    q_sections(page_id).all(),
                    _confusables(session, page_id)
                ) for page_id, cnt, title in pages
            )

            for txt in tpl.generate(
                dump_file=self.dump_file,
                query=page_gen,
                coverage=cov
            ):
                fd.write(txt)
        print("done.", file=sys.stderr)

        return filename

    def _set_dump_file(self, filename):
        self.dump_file = os.path.basename(filename)


def generate(
        db,
        folder=FOLDER,
        max_languages=20,
        max_items=500,
        language=None):
    """
    generate files
    """

    with db.session_scope() as session:
        writer = _Writer(folder=folder,
                         max_items=max_items)
        status = Status.singleton(session)
        if status.lastop != Progress.CONFUSABLES:
            raise UnexpectedLastOp(status.lastop, Progress.CONFUSABLES)
        if status.is_empty() or not status.finished:
            raise PreconditionException(
                "the collection has not been completed")

        writer._set_dump_file(status.dump)

        filename = "%s:Páginas requeridas%s" % (APPENDIX, EXTENSION)

        lang_codes = {c.name.lower(): c.code
                      for c in session.query(LanguageCode)}

        if not language:
            writer._wanted_pages(session, filename)
            languages = Model.q_wanted_languages(session)\
                .limit(max_languages).all()
            languages = [l + (lang_codes.get(l[1].lower()),)
                         for l in languages]
            writer._wanted_sections(
                session,
                FILE_PREFIX + EXTENSION,
                languages
            )
        else:
            language = language.strip()
            sq = Model.q_wanted_languages(session).subquery()
            languages = session.query(sq).filter_by(name=language).all()
            languages = [l + (lang_codes.get(l[1].lower()),)
                         for l in languages]

        for lang in languages:
            filename = FILE_PREFIX + "_SUB_" + lang[1] + EXTENSION
            writer._wanted_lang_sections(session, filename, lang)


def upload(folder=FOLDER, comment=COMMENT, dry=False, language=None):

    if not language:
        pattern = "{}:*{}".format(APPENDIX, EXTENSION)
    else:
        pattern = "{}_SUB_{}{}".format(
            FILE_PREFIX,
            language.strip(),
            EXTENSION
        )

    filenames = sorted(glob.glob(os.path.join(folder, pattern)))

    site = pw.Site()
    print("SITE:", repr(site))

    for idx, filename in enumerate(sorted(filenames)):
        filename = filename.strip()
        if not filename:
            continue
        pagename, ext = os.path.splitext(os.path.basename(filename))
        pagename = pagename.replace("_SUB_", "/")
        print("* %d: PAGENAME[%s]" % (idx, pagename))

        pw_page = pw.Page(site, pagename)
        print("  REMOTE PAGE[%s]" % pw_page.title())

        with codecs.open(filename, encoding="utf-8") as fd:
            text = fd.read().rstrip(" \n")

            if pw_page.exists():
                print("   page exists")
                diff_gen = difflib.unified_diff(
                    pw_page.get().split("\n"),
                    text.split("\n")
                )
                diff = "\n".join(diff_gen)

                if not diff.strip():
                    print("   page is identical. Skipping.")
                    continue

                pydoc.pager(diff)
            else:
                print("   page is new")
            print("- Upload page content (y/N/q) ? ")
            choice = p3_input().lower()
            if choice == "y":
                print("Uploading '%s'" % pagename)
                if not dry:
                    pw_page.text = text
                    pw_page.save(summary=comment, minor=False)
            elif choice == "q":
                return
            else:
                continue
