#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import sys
#import os
#import codecs
import argparse
from datetime import datetime

from . import writer
from . import GraphBot
from .logger import print0
from wikiexpand.wiki.xml_dump import (
    xml_generator,
    indexed_xml_generator,
    XMLPage
)
from ..db import model
from ..db.model import Model
from ..db.context import (
    DbWiki,
    PreconditionException,
    Progress
)
from wikiexpand.compat import p3_str


COMMENT = "actualizando compilación"


# collect dump db
def do_collect(parser, db=None):
    "collect information from the pages in a dump file"

    if not db:
        db = Model.sqlite(parser.db)

    error = False
    with db.session_scope() as session:
        status = model.Status.singleton(session)

        incremental = status.incremental

        has_context = not status.is_empty()

        if has_context and not status.is_context(parser.wiki_dump):
            print0("Dump filename mismatch")
            error = True
        elif status.finished:
            print0("Data has already been collected")
            error = True

        info = repr(status)

    if error:
        print0("DB INFO:", info)
        sys.exit(2)

    if not has_context:
        print0("No context detected.\nBuilding wiki context for '%s' ..." % parser.wiki_dump)
        do_context(parser, db=db)

    print0("Collecting links and sections")
    if incremental:
        gen = indexed_xml_generator(parser.wiki_dump)
    else:
        gen = xml_generator(parser.wiki_dump)
    bot = GraphBot(db, gen)
    bot.run()


def do_expand(parser, db=None):
    " expand a file"
    if not db:
        db = Model.sqlite(parser.db)

    text = parser.text.read()

    page = XMLPage({
        "title": parser.title,
        "text": text,
        "timestamp": datetime.now(),
        "revisionid": 0,
        "isredirect": False,
    })

    with db.session_scope() as session:
        bot = GraphBot(db, None)
        expanded = bot.expand_page(session, page)

    print(expanded, file=sys.stdout)


def do_context(parser, db=None):
    "build context data needed for page expansion"
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        wiki = DbWiki(session)
        wiki.build_context(
            parser.wiki_dump,
            incremental=parser.incremental
        )


def do_transclusion(parser, db=None):
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        wiki = DbWiki(session)
        wiki.collect_transclusions(parser.wiki_dump)


def do_propagate(parser, db=None):
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        wiki = DbWiki(session)
        wiki.propagate_timestamp()


def do_skip_ratio(parser, db=None):
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        wiki = DbWiki(session)
        ratio, affected, total = wiki.skip_ratio()
        print("\nThe %0.2f%% of the main namespace will be revisited (%s / %s)." % (100.0 * (1.0 - ratio), affected, total))


def do_build_index(parser, db=None):
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        wiki = DbWiki(session)
        wiki.generate_index()


def do_write(parser):
    "generate result files"
    db = Model.sqlite(parser.db)
    writer.generate(
        db,
        folder=parser.directory,
        language=parser.language
    )


def do_upload(parser):
    "upload results to wiki"
    writer.upload(
        folder=parser.directory,
        dry=parser.dry_run,
        language=parser.language,
        comment=parser.comment
    )


def do_collect_confusables(parser, db=None):
    "collect confusables"
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        wiki = DbWiki(session)
        wiki.collect_confusables()


def do_disambiguation(parser, db=None):
    "show confusable pages"

    if not db:
        db = Model.sqlite(parser.db)

    lang = None if not parser.language else parser.language.strip()
    with db.session_scope() as session:
        wiki = DbWiki(session)
        for p in wiki.page_confusables(parser.title, lang):
            print(p)


def do_info(parser, db=None):
    "show status information"
    if not db:
        db = Model.sqlite(parser.db)

    with db.session_scope() as session:
        status = model.Status.singleton(session)

        print()
        print("| Current dump:        ", status.dump)
        print("| Dump size:           ", status.size)
        print("| Finished:            ", status.finished)
        print("| Page from:           ", status.page_from)
        print("| Last operation:      ", status.lastop)
        print("| Last edit:           ", status.lastedit)
        print("| Incremental:         ", status.incremental)
        print()
        if status.is_empty():
            print("> Context is empty")
            return
        tr = session.query(model.Transclusion).limit(1).count() > 0
        if tr:
            print("> Transclusion table is not empty")
        if status.finished:
            print("> Collection has finished")
            return
        if status.is_collecting():
            print("> Collection in progress")
            print("* Last collected page: ", status.page_from)


def _get_status(db):
    with db.session_scope() as session:
        status = model.Status.singleton(session)
        return status.lastop, status.finished


def do_prepare(parser, db=None):
    " perform all the previous steps previous to collection"
    if not db:
        db = Model.sqlite(parser.db)

    lastop, finished = _get_status(db)

    if not lastop or (finished and lastop == Progress.CONFUSABLES):
        do_context(parser, db=db)
        lastop, finished = _get_status(db)

    if lastop == Progress.CONTEXT_INC:
        do_transclusion(parser, db=db)
        lastop, finished = _get_status(db)

    if lastop == Progress.TRANSCLUSION:
        do_propagate(parser, db=db)
        lastop, finished = _get_status(db)

    if lastop == Progress.TIMESTAMP:
        do_build_index(parser, db=db)
        lastop, finished = _get_status(db)

    return lastop, finished


def do_run(parser):
    " run the whole process"
    db = Model.sqlite(parser.db)

    lastop, finished = do_prepare(parser, db=db)

    if (lastop == Progress.CONTEXT) or ((lastop == Progress.COLLECTION) and
                                        not finished):
        do_collect(parser, db=db)
        lastop, finished = _get_status(db)

    if (lastop == Progress.COLLECTION) and finished:
        do_collect_confusables(parser, db=db)


def _cmd_db_options(parser):
    group = parser.add_argument_group("database")
    group.add_argument(
        "db",
        type=p3_str,
        nargs="?",
        default="dump.db",
        help="sqlite3 database file used to store data"
    )
    return group


def _cmd_collect_options(parser):
    group = parser.add_argument_group("collect")
    group.add_argument(
        "wiki_dump",
        type=p3_str,
        help="compressed or uncompressed dump file in xml format"
    )
    _cmd_db_options(parser)
    return group


def _cmd_context_options(parser):
    group = parser.add_argument_group("context")
    group.add_argument(
        "-i", "--incremental",
        action="store_true",
        help="incremental mode"
    )
    return group


def _cmd_results_options(parser):
    group = parser.add_argument_group("results")
    group.add_argument(
        "-d", "--directory",
        type=p3_str,
        nargs="?",
        default="results",
        help="directory where to put results"
    )
    group.add_argument(
        "-l", "--language",
        type=p3_str,
        nargs="?",
        help="compile just this language"
    )
    return group


def _cmd_upload_options(parser):
    group = parser.add_argument_group("upload")
    group.add_argument(
        "-r", "--dry-run",
        action="store_true",
        help="dry run"
    )
    group.add_argument(
        "-c", "--comment",
        type=p3_str,
        help="summary",
        default=COMMENT
    )
    return group


def _cmd_expand_options(parser):
    group = parser.add_argument_group("expand")
    group.add_argument(
        "text",
        type=argparse.FileType("r"),
        help="text file to expand"
    )
    group.add_argument(
        "-t", "--title",
        type=p3_str,
        default="foobar",
        help="page title"
    )
    return group


def _cmd_disambiguation_options(parser):
    group = parser.add_argument_group("disambiguation")
    group.add_argument(
        "title",
        type=p3_str,
        help="page title"
    )
    group.add_argument(
        "-l", "--language",
        type=p3_str,
        help="restrict to language"
    )
    return group


def _options():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Bot: Transitive closure by languages"
    )
    subparsers = parser.add_subparsers()

    # info
    cmd_info = subparsers.add_parser(
        "info",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_info.__doc__
    )
    _cmd_db_options(cmd_info)
    cmd_info.set_defaults(func=do_info)

    # run
    cmd_run = subparsers.add_parser(
        "run",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_run.__doc__
    )
    _cmd_collect_options(cmd_run)
    _cmd_context_options(cmd_run)
    cmd_run.set_defaults(func=do_run)

    # prepare
    cmd_prepare = subparsers.add_parser(
        "prepare",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_prepare.__doc__
    )
    _cmd_collect_options(cmd_prepare)
    _cmd_context_options(cmd_prepare)
    cmd_prepare.set_defaults(func=do_prepare)

    # write db folder
    cmd_write = subparsers.add_parser(
        "write",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_write.__doc__
    )
    _cmd_db_options(cmd_write)
    _cmd_results_options(cmd_write)
    cmd_write.set_defaults(func=do_write)

    # upload folder
    cmd_upload = subparsers.add_parser(
        "upload",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_upload.__doc__
    )
    _cmd_results_options(cmd_upload)
    _cmd_upload_options(cmd_upload)
    cmd_upload.set_defaults(func=do_upload)

    # context dump db
    cmd_context = subparsers.add_parser(
        "step_context",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_context.__doc__
    )
    _cmd_collect_options(cmd_context)
    _cmd_context_options(cmd_context)
    cmd_context.set_defaults(func=do_context)

    # transclusion dump db
    cmd_transclusion = subparsers.add_parser(
        "step_transclusion",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_transclusion.__doc__
    )
    _cmd_collect_options(cmd_transclusion)
    cmd_transclusion.set_defaults(func=do_transclusion)

    # propagate dump db
    cmd_propagate = subparsers.add_parser(
        "step_propagate",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_propagate.__doc__
    )
    _cmd_db_options(cmd_propagate)
    cmd_propagate.set_defaults(func=do_propagate)

    # collect dump db
    cmd_collect = subparsers.add_parser(
        "step_collect",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_collect.__doc__
    )
    _cmd_collect_options(cmd_collect)
    cmd_collect.set_defaults(func=do_collect)

    # skip ratio
    cmd_skip = subparsers.add_parser(
        "skip_ratio",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_skip_ratio.__doc__
    )
    _cmd_db_options(cmd_skip)
    cmd_skip.set_defaults(func=do_skip_ratio)

    # skip ratio
    cmd_index = subparsers.add_parser(
        "build_index",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_build_index.__doc__
    )
    _cmd_db_options(cmd_index)
    cmd_index.set_defaults(func=do_build_index)

    # expand
    cmd_expand = subparsers.add_parser(
        "expand",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_expand.__doc__
    )
    _cmd_expand_options(cmd_expand)
    _cmd_db_options(cmd_expand)
    cmd_expand.set_defaults(func=do_expand)

    # collect confusables
    cmd_confusables = subparsers.add_parser(
        "collect_confusables",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_collect_confusables.__doc__
    )
    _cmd_db_options(cmd_confusables)
    cmd_confusables.set_defaults(func=do_collect_confusables)

    # disambiguation
    cmd_disambiguation = subparsers.add_parser(
        "disambiguation",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        help=do_disambiguation.__doc__
    )
    _cmd_disambiguation_options(cmd_disambiguation)
    _cmd_db_options(cmd_disambiguation)
    cmd_disambiguation.set_defaults(func=do_disambiguation)

    return parser


def main():
    try:
        parser = _options()

        args = parser.parse_args()
        if len(args.__dict__) < 1:
            parser.print_help()
            parser.exit(2)

        args.func(args)

    except PreconditionException as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    #except Exception as e:
        #print(e, file=sys.stderr)


if __name__ == "__main__":
    main()
