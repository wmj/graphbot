#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

import os
import pkg_resources as pkg
import mwparserfromhell as mw
#from ..expand.compat import p3_str
from wikiexpand.expand import tools


def _str_number(text):
    ndigits = 0
    for char in text:
        if not char.isdigit():
            break
        ndigits += 1
    return ndigits


def noop(*args, **kwargs):
    return mw.parse(None)


def str_number(params, fn, ctx, *args):
    text = tools.strip(params.get("1", ""))

    ndigits = _str_number(text)
    return tools.str2wikicode(ndigits)


def forma_verbo_pronominales(params, fn, ctx, frame):
    # function
    module = tools.strip(params.get("1", ""))
    if module != "main":
        raise NotImplementedError("lua function: " + module)

    # arg 1
    verb = tools.strip(params.get("2", ""))
    if not verb:
        return mw.parse(None)

    # parent arg leng
    lang = tools.strip(frame.get("leng", "es"))
    if lang != "es":
        return mw.parse(None)

    # parent arg 2 or arg p
    arg2 = tools.strip(frame.get("2", frame.get("p", "")))
    if arg2 in ("inf", "infinitivo", "ger", "gerundio"):
        return mw.parse(None)

    if verb.endswith("se"):
        return mw.parse(None)

    refl = verb + "se"

    if ctx.site_context().page_exists(refl):
        return tools.str2wikicode("s")

    return mw.parse(None)


class LuaString(object):

    MODULE = None

    @classmethod
    def call(cls, params, fn, ctx, frame):
        """
        params - template params
        fn - template expander
        ctx - wiki context
        frame - arguments
        """
        n_module = tools.strip(params.get("1", ""))
        f_module = cls.MODULE.get(n_module)
        if not f_module:
            raise NotImplementedError("lua function: {module}, {params}, {frame}".format(
                module=n_module,
                params=params,
                frame=frame
            ))

        return f_module(params, fn, ctx, frame)

    @classmethod
    def subrev(cls, params, fn, ctx, frame):
        text = tools.strip(params.get("2", ""))

        if not text:
            return mw.parse(None)

        size = len(text)

        if not size:
            return mw.parse(None)

        t_from = tools.strip(params.get("3", ""))

        try:
            first = -int(t_from)
        except ValueError:
            first = None

        if not first or (first > 1):
            first = -1

        if first < -size:
            return mw.parse(None)

        t_len = tools.strip(params.get("4", ""))

        try:
            t_len = int(t_len)
        except ValueError:
            t_len = None

        if not t_len or (t_len < 1):
            t_len = 1

        last = first + t_len

        if last > 0:
            return mw.parse(None)
        elif last == 0:
            out = text[first:]
        else:
            out = text[first:last]

        return tools.str2wikicode(out)

    @classmethod
    def crop(cls, params, fn, ctx, frame):
        text = tools.strip(params.get("2", ""))

        if not text:
            return mw.parse(None)

        t_len = tools.strip(params.get("3", ""))

        try:
            t_len = int(t_len)
        except ValueError:
            t_len = 0

        if 0 < t_len <= len(text):
            text = text[:-t_len]

        return tools.str2wikicode(text)

    @classmethod
    def cropleft(cls, params, fn, ctx, frame):
        text = tools.strip(params.get("2", ""))

        if not text:
            return mw.parse(None)

        t_len = tools.strip(params.get("3", ""))

        try:
            t_len = int(t_len)
        except ValueError:
            t_len = 0

        size = len(text)
        if 0 <= t_len < size:
            text = text[-(size - t_len):]
        elif t_len >= size:
            return mw.parse(None)

        return tools.str2wikicode(text)

    @classmethod
    def left(cls, params, fn, ctx, frame):
        text = tools.strip(params.get("2", ""))

        if not text:
            return mw.parse(None)

        t_len = tools.strip(params.get("3", ""))

        try:
            t_len = int(t_len)
        except ValueError:
            return mw.parse(None)

        if 0 < t_len <= len(text):
            text = text[:t_len]

        return tools.str2wikicode(text)

    @classmethod
    def right(cls, params, fn, ctx, frame):
        text = tools.strip(params.get("2", ""))

        if not text:
            return mw.parse(None)

        t_len = tools.strip(params.get("3", ""))

        try:
            t_len = int(t_len)
        except ValueError:
            return mw.parse(None)

        if 0 < t_len <= len(text):
            text = text[-t_len:]

        return tools.str2wikicode(text)


LuaString.MODULE = {
    "subrev": LuaString.subrev,
    "crop": LuaString.crop,
    "cropleft": LuaString.cropleft,
    "left": LuaString.left,
    "right": LuaString.right,
}


PARSED = {}


LOCAL_TEMPLATES = "local_templates"

CTX_TEMPLATE = os.path.join(LOCAL_TEMPLATES, "contexto.wiki")

with pkg.resource_stream(__name__, CTX_TEMPLATE) as fd:
    PARSED["contexto"] = tools.parse(fd)


def contexto(params, fn, ctx, frame):
    return fn(PARSED["contexto"], params)


TEMPLATES = {
    "str number": str_number,
    "forma verbo/pronominales": forma_verbo_pronominales,
    "String": LuaString.call,
    "cita publicación": noop,
    "coord": noop,
    "contexto": contexto,
    "contexto 1": contexto,
    "contexto 2": contexto,
    "contexto 3": contexto,
    "contexto 4": contexto,
    "contexto 5": contexto,
    "contexto 6": contexto,
    "contexto 7": contexto,
    "contexto 8": contexto,
    "contexto 9": contexto,
    "contexto 10": contexto,
}
