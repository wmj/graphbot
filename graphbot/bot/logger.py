#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import sys
import time
from datetime import datetime, timedelta
from functools import partial

from wikiexpand.compat import p3_str


print0 = partial(print, file=sys.stderr)


MSG_SPEED = """
[{time}]

CURRENT SPEED: {v_i:0.2f} pages / minute
E.T.A.: {eta_i}

ELAPSED TIME: {elapsed}

AVERAGE SPEED: {v_t:0.2f} pages / minute
E.T.A.: {eta_t}
"""


MSG_ITEM = "{visited:d} ({ratio:0.2f}%) - [ relevant {affected:d} ({aff_pct:0.2f}%) / processed {treated:d} ({proc_pct:0.2f}%) / visited {current:d} / total {total:d} ] {title}"


class SpeedLogger(object):

    THRESHOLD = 15

    def __init__(self, total_units):
        self._total_units = total_units
        self._total_elapsed = 0.0
        self._units = 0.0
        now = self.restart_clock()
        self._tic = now
        self._last_update = now

        # initProcessed
        self._processed = 0
        # counter
        self._visited = 0
        # counterV
        self._treated = 0
        # counterP
        self._affected = 0

        self._title = ""

    def restart_clock(self):
        self._init_time = time.time()
        return self._init_time

    def processed_item(self):
        self._processed += 1

    def treated_item(self, affected=1):
        self._treated += 1
        self._affected += affected

    def next_item(self, title):
        self._title = title
        self._visited += 1
        self._tick()

    def _tick(self):
        toc = time.time()
        elapsed = toc - self._tic
        self._total_elapsed += elapsed
        self._units += 1
        self._tic = toc

        self._update_speed()

    def _update_speed(self):
        if (self._tic - self._last_update) > self.THRESHOLD:
            self._last_update = self._tic
            avg_speed = self._tic - self._init_time
            remaining_units = self._total_units - self._visited
            v_i = 60.0 * self._units / self._total_elapsed
            eta_i = (timedelta(seconds=(remaining_units * 60.0) / v_i)
                     if v_i > 0 else float("inf"))
            v_t = 60.0 * self._processed / avg_speed
            eta_t = (timedelta(seconds=(remaining_units * 60.0) / v_t)
                     if v_t > 0 else float("inf"))

            if self._units > 20:
                self._total_elapsed = 0.0
                self._units = 0

            print0(MSG_SPEED.format(
                time=datetime.now(),
                elapsed=p3_str(timedelta(seconds=avg_speed)),
                v_i=v_i,
                v_t=v_t,
                eta_i=p3_str(eta_i),
                eta_t=p3_str(eta_t)
            ))
            self.show(self._title)

    def show(self, txt):
        print0(MSG_ITEM.format(
            visited=self._visited,
            ratio=100.0 * self._visited / self._total_units,
            title=txt,
            affected=self._affected,
            aff_pct=100.0 * self._affected / max(self._treated, 1),
            treated=self._treated,
            proc_pct=100.0 * self._treated / self._visited,
            current=self._processed,
            total=self._total_units
        ))
