#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

#import pywikibot as pw
import mwparserfromhell as mw
import codecs
import re
#import sys
#from datetime import datetime

from wikiexpand.compat import p3_str
from wikiexpand.expand import ExpansionContext
from ..db import model
from ..db.model import Page, Heading, Section
from ..db.context import DbWiki, UnexpectedLastOp, Progress
from ..db.templates import TemplateDB
from .templates import TEMPLATES

#import profilestats

from .logger import SpeedLogger, print0


class GraphBot(object):

    def __init__(self, db, generator, pw_site=None, **kwargs):
        self.db = db
        self.generator = generator

        self.last_dump = None

        self.namespaces = (0,)

        # template store
        templates = TemplateDB(None)
        templates.callable_templates.update(TEMPLATES)
        # wiki context
        self.wiki = DbWiki(session=None, pw_site=pw_site)
        # expansion context
        self.expander = ExpansionContext(templates)

    def run(self):
        step = 15

        with self.db.session_scope() as session:
            status = model.Status.singleton(session)
            page_from = status.page_from

            self.last_dump = status.lastedit

            expected_op = (Progress.CONTEXT, Progress.COLLECTION)
            if status.lastop not in expected_op:
                raise UnexpectedLastOp(status.lastop, ", ".join(expected_op))

            log = SpeedLogger(status.size)

        if page_from:
            print0("FROM:", page_from)

        for idx, (cnt, page) in enumerate(self.generator, start=1):
            title = page.title if page else None
            if page_from is not None:
                if page_from == title:
                    page_from = None
                    log.restart_clock()
            else:
                not_redirect = page and not page.isredirect
                in_namespace = page and (page.ns in self.namespaces)
                if not_redirect and in_namespace:
                    with self.db.session_scope() as session:
                        status.page_from = title
                        session.merge(status)
                        if idx % step == 0:
                            log.show(title)
                        affected = self.process_page(session, page)
                        log.treated_item(affected)

                log.processed_item()
            log.next_item(title)

        # finish
        with self.db.session_scope() as session:
            status.finished = True
            status.lastop = Progress.COLLECTION
            status.lastedit = Page.get_last_edit(session)
            session.merge(status)

        print0("FINISHED")

    #@profilestats.profile()
    def process_page(self, session, page):
        page_title = page.title
        db_page = Page.find(session, name=page_title)

        # update db
        dirty = self.prepare_page(session, db_page, page.timestamp)

        if not dirty:
            # already processed in a previous dump
            return dirty

        # expand text
        txt = self.expand_page(session, page)

        if txt is None:
            return 0

        try:
            ok = self.analyze_page(session, db_page, txt)
        except:
            print0("PAGE", page_title)
            raise
        if not ok:
            session.rollback()
            return 0

        return 1

    def prepare_page(self, session, db_page, t_new):
        # pagina nueva -> procesa
        # pagina vieja -> resetea y procesa
        # nuevo timestamp -> resetea y procesa
        is_new = db_page.is_new()
        is_obsolete = db_page.is_obsolete(self.last_dump)

        update = (
            is_new or
            is_obsolete or
            (t_new > (self.last_dump or db_page.lastedit))
        )

        if update:
            if not is_new:
                db_page.reset(session, flush=True)

            db_page.lastedit = max(t_new, db_page.related_timestamp())
            session.merge(db_page)

        return int(update)

    def expand_page(self, session, page):
        self.wiki.session = session
        self.expander.templates.session = session
        page_context = self.wiki.page_context(page.title)
        self.expander.set_context(page_context)

        txt = None
        try:
            txt = self.expander.expand(page.text)
        except mw.parser.ParserError:
            if txt:
                with codecs.open("graphbot_%s.wiki" % page.id, "w", "utf-8") as fd:
                    fd.write(page.title)
                    fd.write("\n")
                    fd.write(p3_str(txt))
        except:
            print0("PAGE " + page.title)
            raise

        return txt

    def analyze_page(self, session, db_page, txt):
            # clean text
            #txt = self.wiki.clean_text(p3_str(txt))
            #txt = p3_str(txt)

            page_context = self.expander.context
            page_title = db_page.name

            try:
                # force parsing without skipping style tags
                #txt = mw.parse(p3_str(txt))
                txt = mw.parse(txt)
            except mw.parser.ParserError:
                if txt:
                    with codecs.open("graphbot_%s.wiki" % db_page.id, "w", "utf-8") as fd:
                        fd.write(page_title)
                        fd.write("\n")
                        fd.write(p3_str(txt))
                return False

            # extract headings

            # collections to insert or ignore
            c_headings = set(x for _, x in iter_headings(page_context, txt))
            c_self_sections = list(c_headings)  # set blue
            c_links = set()
            c_sections = set((page_title, x) for x in c_self_sections)
            c_section_links = set()

            # extract sections

            for l_name, l_ns, l_section in page_context.wikilinks(txt, clean_text=False):
                if l_ns in self.namespaces:
                    autolink = not l_section and (l_name == page_title)
                    if not autolink:
                        c_links.add(l_name)

                        if l_section:
                            c_headings.add(l_section)
                            c_sections.add((l_name, l_section))
                            c_section_links.add((l_name, l_section))

            # insert pages
            ret_pages = Page.insert_or_ignore(session, c_links)
            ret_pages[page_title] = db_page.id

            # insert headings
            ret_headings = Heading.insert_or_ignore(session, c_headings)

            # insert links
            db_page.insert_or_ignore_links(session, c_links)

            # insert sections
            Section.insert_or_ignore(
                session,
                [{"page_id": ret_pages[x], "heading_id": ret_headings[y]}
                 for x, y in c_sections]
            )

            # insert self sections (blue)
            db_page.set_existing_sections(session, c_self_sections)

            # insert section links
            db_page.insert_or_ignore_section_links(
                session,
                c_section_links
            )

            #show_numbers(session, page_title)

            return True


R_RELATED_HEADING = re.compile(r"([^\(]+)\s+\(([^\)]*)\)")


def iter_headings(ctx, txt):
    for lvl, name in ctx.headings(txt, clean_text=True):
        if name.endswith(")"):
            m = R_RELATED_HEADING.match(name)
            if m:
                yield lvl, m.group(1)

        yield lvl, name


def show_numbers(session, page_title):

    print()
    print(page_title)
    print("=" * len(page_title))

    r = session.execute("select count(*) from pages")
    print("count pages", r.fetchall())

    r = session.execute("select count(*) from headings")
    print("count headings", r.fetchall())

    r = session.execute("select from_id, count(*) from links group by from_id")
    print("links by source", r.fetchall())

    r = session.execute("select page_id, count(*) from sections where blue group by page_id")
    print("blue sections by page", r.fetchall())

    r = session.execute("select count(*) from sections")
    print("count sections", r.fetchall())

    r = session.execute("select from_id, count(*) from section_links group by from_id")
    print("sectionlinks by source", r.fetchall())
