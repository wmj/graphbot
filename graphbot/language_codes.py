#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

import sys
import argparse
import mwparserfromhell as mw
import pywikibot as pw
from wikiexpand.compat import p3_str

from .db.model import Model
from .db.context import DbWiki


def parse_code_table(text):
    tree = mw.parse(text)

    tables = (tag for tag in tree.ifilter_tags(recursive=False)
              if tag.tag == "table")

    try:
        table = next(tables)
    except StopIteration:
        return

    rows = (row.contents.nodes[:2]
            for row in table.contents.nodes
            if row.tag == "tr")

    for code, name in rows:
        code = p3_str(code.contents).strip()
        name = p3_str(name.contents).strip()

        yield code, name


def _options():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Update language codes"
    )
    parser.add_argument(
        "-p", "--page",
        type=p3_str,
        default="Apéndice:Códigos de idioma",
        help="Page name with language code table"
    )
    parser.add_argument(
        "db",
        type=p3_str,
        nargs="?",
        default="dump.db",
        help="sqlite3 database file used to store data"
    )

    return parser


def cmd(db_filename, pagename):
    db = Model.sqlite(db_filename)

    site = pw.Site()
    page = pw.Page(site, pagename)
    text = page.get()

    with db.session_scope() as session:
        wiki = DbWiki(session)
        wiki.insert_language_codes(parse_code_table(text))


def main():
    args = _options().parse_args()

    cmd(args.db, args.page)


if __name__ == "__main__":
    main()
