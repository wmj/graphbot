#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import (
    unicode_literals,
    print_function,
    absolute_import,
    division
)

import json
import sys
import os
import codecs
import unicodedata as ud
import pkg_resources as pkg

from wikiexpand.compat import p3_chr


def code2uchar(text):
    return "".join(p3_chr(int(x, 16)) for x in text.split())


CTX_DATA = os.path.join("data", "confusables.json")


def generate(filename):
    translation = {}
    description = []

    with codecs.open(filename, "r", "utf-8-sig") as fd:
        for line in fd:
            line = line.strip("\n")
            if not line or not line.startswith("#"):
                break
            description.append(line)

        for line in fd:
            line = line.strip("\n")
            if not line or line.startswith("#"):
                continue

            parts = line.split(";")
            source = code2uchar(parts[0].strip())
            target = code2uchar(parts[1].strip())

            assert source not in translation
            translation[source] = target

    return {
        "description": "\n".join(description),
        "translation": translation
    }


class ConfusableHash(object):

    IGNORE_CHARS = "-' "

    def __init__(self, fd=None, *args, **kwargs):
        if fd:
            self.table = json.load(fd)["translation"]
        else:
            with open(pkg.resource_filename(__name__, CTX_DATA)) as fd:
                #print(repr(fd))
                self.table = json.load(fd)["translation"]

    def transform(self, word):
        word = ud.normalize("NFD", word)
        chars = (self.table.get(c, c) for c in word)

        return "".join(chars)

    def hash1(self, nfd_word):
        chars = "".join(self.table.get(c, c) for c in nfd_word)
        chars = [c for c in chars if not ud.combining(c)]

        if len(chars) > 1:
            return "".join(x for x in chars if x not in self.IGNORE_CHARS)
        else:
            return "".join(chars)

    def hash2(self, nfd_word):
        chars = [self.table.get(c, c) for c in nfd_word if not ud.combining(c)]
        if len(chars) > 1:
            return "".join(x for x in chars if x not in self.IGNORE_CHARS)
        else:
            return "".join(chars)

    def hash_strings(self, word):
        # estrategia:
        # - 1. palabra tal cual
        # - 2. minusculas
        # - 3. mayúsculas
        #
        # para cada:
        #
        # limpiar marcas,  confusables
        # confusables, limpiar marcas

        words = {word, word.lower(), word.upper()}

        out = set()

        for w in words:
            nw = ud.normalize("NFD", w)
            out.update((self.hash1(nw), self.hash2(nw)))

        return [x for x in out if x]


def main():
    data = generate(sys.argv[1])
    return json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
