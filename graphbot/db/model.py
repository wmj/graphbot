#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

from contextlib import contextmanager
from datetime import datetime

from sqlalchemy.engine import Engine
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import sessionmaker, relationship
import sqlalchemy.sql.expression as sql
from sqlalchemy import (
    bindparam,
    create_engine,
    func,
    not_,
    #select,
    column,
    Column,
    Integer,
    String,
    Text,
    DateTime,
    Boolean,
    #Float,
    ForeignKey,
    PrimaryKeyConstraint,
    UniqueConstraint
)
from wikiexpand.compat import p3_str
from . import extensions as ext
import sys


class BaseTable(object):

    @classmethod
    def find(cls, session, **kwargs):
        try:
            el = session.query(cls).filter_by(**kwargs).one_or_none()
        except:
            print("FIND ERROR", repr(cls), repr(kwargs), file=sys.stderr)
            raise
        if el is None:
            el = cls(**kwargs)
            session.add(el)
        return el


Base = declarative_base(cls=BaseTable)


class TemplateCache(Base):
    __tablename__ = "template_cache"
    name = Column(String, primary_key=True)
    body = Column(Text)
    redirect_name = Column(
        String,
        ForeignKey("template_cache.name"),
        index=True,
        nullable=True
    )

    redirection = relationship(
        "TemplateCache",
        remote_side=[name],
        post_update=True
    )


class PageInc(Base):
    __tablename__ = "pages_inc"
    name = Column(String, primary_key=True)
    namespace = Column(Integer, nullable=False, default=0, server_default="0")
    lastedit = Column(DateTime)
    is_new = Column(Boolean, nullable=False, default=False, server_default="0")
    xmlid = Column(Integer, unique=True)
    xmlorder = Column(Integer, unique=True)

    @classmethod
    def query_update_is_new(cls, session):
        t = cls.__table__
        q1 = session.query(Page.name)
        q = t.update().values(is_new=True).where(t.c.name.notin_(q1))
        return session.execute(q)


class Page(Base):
    __tablename__ = "pages"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    lastedit = Column(DateTime)
    ext_lastedit = Column(DateTime)
    namespace = Column(Integer, nullable=False, default=0, server_default="0")
    visited = Column(Boolean, nullable=False, default=False, server_default="0")
    xmlid = Column(Integer, unique=True)
    xmlorder = Column(Integer, unique=True)

    templates = association_proxy("transclusions", "template")

    def __repr__(self):
        return "<Page('%s', visited=%s)>" % (self.name, self.visited)

    #def is_new(self):
        #return self.id is None

    def reset(self, session, flush=True):
        _id = self.id
        t_section = Section.__table__
        t_slink = SectionLink.__table__
        t_link = Link.__table__

        # reset page sections
        q1 = t_section.update()\
            .where(t_section.c.page_id == _id)\
            .values(blue=False)
        session.execute(q1)

        # delete older sections
        q2 = t_slink.delete().where(t_slink.c.from_id == _id)
        session.execute(q2)

        # delete older links
        q3 = t_link.delete().where(t_link.c.from_id == _id)
        session.execute(q3)

        if flush:
            session.flush()

    def set_link(self, session, target):
        return Link.find(session, source=self, target=target)

    def set_section(self, session, heading):
        return Section.find(session, page=self, heading=heading)

    def related_timestamp(self):
        return self.ext_lastedit or datetime.min

    def is_new(self):
        return not self.lastedit

    def is_obsolete(self, threshold=None):
        if not threshold:
            return not self.is_new() and (self.related_timestamp() > self.lastedit)
        else:
            return not self.is_new() and (self.related_timestamp() > threshold)

    def confusables(self, session):
        return self.q_confusables(session, self.id)

    def section_confusables(self, session, heading):
        return self.q_section_confusables(session, self.id, heading.id)

    @classmethod
    def q_confusables(cls, session, page_id):
        q_self_key = session.query(UnicodeHash.u_key)\
            .filter_by(page_id=page_id)
        q_n = session.query(UnicodeHash.page_id)\
            .filter(UnicodeHash.u_key.in_(q_self_key))\
            .subquery("grupo")
        q = session.query(cls.name)\
            .filter(cls.id.in_(q_n))\
            .filter(cls.id != page_id)

        return q.order_by(cls.name)

    @classmethod
    def q_section_confusables(cls, session, page_id, section_id):
        q_self_key = session.query(UnicodeHash.u_key)\
            .filter_by(page_id=page_id)
        q_n = session.query(UnicodeHash.page_id)\
            .filter(UnicodeHash.u_key.in_(q_self_key))\
            .subquery("grupo")
        q = session.query(cls.name)\
            .filter(cls.id.in_(q_n))\
            .filter(cls.id != page_id)\
            .join(Section, Section.page_id == cls.id)\
            .filter_by(heading_id=section_id)

        return q.order_by(cls.name)

    @classmethod
    def get_last_edit(cls, session):
        q = session.query(func.max(cls.lastedit))
        return q.scalar()

    @classmethod
    def query_by_links(cls, session):
        q = session.query(cls, func.count("*").label("occurrences"))\
            .join(cls.links_from)\
            .group_by(cls.id)
        return q

    @classmethod
    def total_visited(cls, session):
        q = session.query(func.count(cls.id)).filter(cls.visited)
        return q.scalar()

    @classmethod
    def coverage(cls, session, namespace=0):
        q1 = session.query(func.count(cls.id))\
            .filter(cls.visited)\
            .filter(cls.namespace == namespace)

        q2 = session.query(func.count(cls.id))\
            .filter(cls.namespace == namespace)

        return (q1.scalar(), q2.scalar())

#    @classmethod
#    def insert_or_ignore(cls, session, names):
#
#        colname = cls.__table__.c.name
#
#        # find existing items
#        q_sel = select([colname]).\
#            where(colname.in_(names))
#
#        existing = set(x for x, in session.execute(q_sel))
#
#        new_items = set(names) - existing
#
#        if new_items:
#            session.bulk_insert_mappings(cls, [{"name": x} for x in new_items])
#
#        return new_items

    @classmethod
    def insert_or_ignore(cls, session, names):

        if not names:
            return {}

        q_ins = cls.__table__.insert().prefix_with("OR IGNORE")

        session.execute(q_ins, [{"name": x} for x in names])

        q_sel = session.query(cls.id, cls.name)\
            .filter(cls.name.in_(names))

        return {name: id for id, name in q_sel.all()}

    def insert_or_ignore_links(self, session, names):

        if not names:
            return

        q0 = session.query(Link.to_id)\
            .filter(Link.from_id == self.id)

        q_sel = session.query(sql.literal(self.id), Page.id)\
            .filter(Page.name.in_(names))\
            .filter(Page.id.notin_(q0))

        t_link = Link.__table__
        q = t_link\
            .insert()\
            .from_select([t_link.c.from_id, t_link.c.to_id], q_sel)

        session.execute(q)

    def set_existing_sections(self, session, names):
        if not names:
            return

        q0 = session.query(Heading.id)\
            .filter(Heading.name.in_(names))

        t = Section.__table__
        q = t.update()\
            .values(blue=True)\
            .where(t.c.page_id == self.id)\
            .where(t.c.heading_id.in_(q0))

        session.execute(q)

    __ioisl_q = """
        INSERT OR IGNORE INTO section_links(from_id, section_id)
        WITH t_links(page_name, heading_name) AS ({values})
        SELECT :page_id, s.id
            FROM sections AS s
            JOIN pages AS p
                ON s.page_id = p.id
            JOIN headings AS h
                ON s.heading_id = h.id
            JOIN t_links AS t
                ON t.page_name = p.name AND t.heading_name = h.name
    """

    def insert_or_ignore_section_links(self, session, tuples):

        if not tuples:
            return

        param_names = [
            (bindparam("p_%d" % idx, x), bindparam("h_%d" % idx, y))
            for idx, (x, y) in enumerate(tuples)
        ]

        #print([(p3_str(x), p3_str(y)) for x, y in param_names])

        v = ext.values(
            [column("page_name", String),
             column("section_name", String)],
            *param_names
        )

        q = self.__ioisl_q.format(values=p3_str(v))

        params = {p.key: p.value for x in param_names for p in x}
        params["page_id"] = self.id

        session.execute(q, params)


class Link(Base):
    __tablename__ = "links"
    id = Column(Integer, primary_key=True)
    from_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    to_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    UniqueConstraint(from_id, to_id)

    source = relationship("Page", backref="links", foreign_keys=[from_id])
    target = relationship("Page", backref="links_from", foreign_keys=[to_id])

    def __repr__(self):
        return "<Link(from_id=%d,to_id=%d)>" % (self.from_id,
                                                self.to_id)


class Heading(Base):
    __tablename__ = "headings"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return "<Heading(name=%s)>" % self.name

    @classmethod
    def q_annotated_headings(cls, session, parent_name):
        q_string = "{0} (%)".format(parent_name)
        return session.query(cls)\
            .filter(cls.name.like(q_string))\
            .order_by(cls.name)

#    @classmethod
#    def insert_or_ignore(cls, session, names):
#
#        colname = cls.__table__.c.name
#
#        # find existing items
#        q_sel = select([colname]).\
#            where(colname.in_(names))
#
#        existing = set(x for x, in session.execute(q_sel))
#
#        new_items = set(names) - existing
#
#        if new_items:
#            session.bulk_insert_mappings(cls, [{"name": x} for x in new_items])
#
#        return new_items

    @classmethod
    def insert_or_ignore(cls, session, names):

        if not names:
            return

        q_ins = cls.__table__.insert().prefix_with("OR IGNORE")

        session.execute(q_ins, [{"name": x} for x in names])

        q_sel = session.query(cls.id, cls.name)\
            .filter(cls.name.in_(names))

        return {name: id for id, name in q_sel.all()}


class Section(Base):
    __tablename__ = "sections"
    id = Column(Integer, primary_key=True)
    page_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    heading_id = Column(
        Integer,
        ForeignKey("headings.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    blue = Column(Boolean, default=False, server_default="0")
    UniqueConstraint(page_id, heading_id)

    page = relationship("Page", backref="sections")
    heading = relationship("Heading")

    def __repr__(self):
        return "<Section(page=%d,heading=%d,blue=%s)>" % (self.page_id,
                                                          self.heading_id,
                                                          self.blue)

    def set_link_from(self, session, page):
        return SectionLink.find(session, section=self, page=page)

    @classmethod
    def query_by_links(cls, session, page_id=None):
        q = session.query(cls, func.count("*").label("occurrences"))\
            .join(cls.section_links_from)\
            .group_by(cls.id)
        if page_id is not None:
            return q.filter(cls.page_id == page_id)
        return q

    @classmethod
    def coverage(cls, session, heading_id=None):
        q1 = session.query(func.count(cls.id))\
            .filter(cls.blue)

        q2 = session.query(func.count(cls.id))

        if heading_id:
            q1 = q1.filter(cls.heading_id == heading_id)
            q2 = q2.filter(cls.heading_id == heading_id)

        return (q1.scalar(), q2.scalar())

    @classmethod
    def insert_or_ignore(cls, session, id_tuples):
        q_ins = cls.__table__.insert().prefix_with("OR IGNORE")

        session.execute(q_ins, id_tuples)


class SectionLink(Base):
    __tablename__ = "section_links"
    from_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    section_id = Column(
        Integer,
        ForeignKey("sections.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    PrimaryKeyConstraint(from_id, section_id)

    page = relationship("Page")
    section = relationship("Section", backref="section_links_from")

    @classmethod
    def query_by_links(cls, session):
        q = session.query(cls, func.count("*").label("occurrences"))\
            .join(cls.section_links_from)\
            .group_by(cls.id)
        return q

    def __repr__(self):
        return "<SectionLink(from_id=%d,section_id=%d)>" % (self.from_id,
                                                            self.section_id)


class Status(Base):
    __tablename__ = "status"
    id = Column(Integer, primary_key=True)
    page_from = Column(String)
    dump = Column(String)
    finished = Column(Boolean, nullable=False, default=False, server_default="0")
    size = Column(Integer, nullable=False, default=0, server_default="0")
    lastop = Column(String)
    lastedit = Column(DateTime)
    incremental = Column(Boolean, nullable=False, default=False, server_default="0")

    @classmethod
    def singleton(cls, session):
        return cls.find(session, id=0)

    def __repr__(self):
        return "<Status(finished=%s,page_from=%s,size=%s,dump=%s,lastop=%s,lastedit=%s,incremental=%s)>" % (
            repr(self.finished),
            repr(self.page_from),
            repr(self.size),
            repr(self.dump),
            repr(self.lastop),
            repr(self.lastedit),
            repr(self.incremental)
        )

    def reset(self, dump):
        self.dump = dump
        self.page_from = None
        self.finished = False
        self.size = None
        self.lastop = None
        self.lastedit = None
        self.incremental = False

    def is_empty(self):
        return not self.dump

    def is_context(self, dump):
        return self.dump == dump

    def is_collecting(self):
        return not self.finished and (self.page_from is not None)


class Transclusion(Base):
    __tablename__ = "transclusion"
    page_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    template_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        index=True,
        nullable=False
    )
    PrimaryKeyConstraint(page_id, template_id)

    page = relationship(
        "Page",
        foreign_keys=[page_id],
        backref="transclusions"
    )
    template = relationship(
        "Page",
        foreign_keys=[template_id],
        backref="transcluded_by"
    )


class UnicodeHash(Base):
    __tablename__ = "unicode_hash"
    page_id = Column(
        Integer,
        ForeignKey("pages.id", ondelete="CASCADE"),
        nullable=False,
        primary_key=True
    )
    u_key = Column(
        String,
        nullable=False,
        primary_key=True,
        index=True
    )
    #UniqueConstraint(page_id, u_key)
    page = relationship(
        "Page",
        foreign_keys=[page_id],
        backref="unicode_hashes"
    )


class LanguageCode(Base):
    __tablename__ = "language_codes"
    code = Column(
        String,
        nullable=False,
        primary_key=True
    )
    name = Column(
        String,
        nullable=False,
        index=True
    )

    def __repr__(self):
        return "<LanguageCode(code=%s, name=%s)>" % (self.code, self.name)


#*******************************

#@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    try:
        cursor.execute("PRAGMA foreign_keys=1")
        #cursor.execute("PRAGMA journal_mode=PERSIST")
    except:
        pass
    cursor.close()


class Model(object):

    @classmethod
    def sqlite(cls, db_name, echo=False):
        event.listen(Engine, "connect", set_sqlite_pragma)
        engine = create_engine('sqlite:///' + db_name, echo=echo)
        return cls(engine)

    @classmethod
    def pgsql(cls, db_name, user, password, echo=False):
        engine = create_engine(
            "postgresql://%s:%s@localhost:5432/%s" % (user, password, db_name),
            echo=echo
        )
        return cls(engine)

    def __init__(self, engine):
        self.engine = engine
        Base.metadata.create_all(engine)
        self.Session = sessionmaker(bind=engine)

    def session(self):
        return self.Session()

    @contextmanager
    def session_scope(self):
        session = self.session()
        try:
            yield session
            session.commit()
        except:
            if session.transaction and session.transaction.is_active:
                session.rollback()
            raise
        finally:
            session.close()

    @classmethod
    def q_wanted_languages(cls, session):
        s_by_l = Section.query_by_links(session).\
            subquery("section_by_links")

        sq = session.query(s_by_l.c.heading_id,
                           func.sum(s_by_l.c.occurrences).label("cnt"))\
            .filter(sql.not_(s_by_l.c.blue))\
            .group_by(s_by_l.c.heading_id)\
            .subquery("sq")

        q = session.query(Heading.id, Heading.name, sq.c.cnt)\
            .join(sq, Heading.id == sq.c.heading_id)\
            .order_by(sq.c.cnt.desc(), Heading.name)

        return q

    @classmethod
    def q_wanted_sections(cls, session, heading=None):
        s_by_l = Section.query_by_links(session).\
            subquery("section_by_links")

        lbl_id = s_by_l.c.page_id.label("page_id")
        lbl_oc = s_by_l.c.occurrences.label("occurrences")
        lbl_title = Page.name.label("title")
        lbl_section = Heading.name.label("section")
        lbl_hid = Heading.id.label("heading_id")
        q = session.query(lbl_oc, lbl_title, lbl_section, lbl_id, lbl_hid)\
            .filter(sql.not_(s_by_l.c.blue))\
            .join(Page, Page.id == s_by_l.c.page_id)\
            .join(Heading, Heading.id == s_by_l.c.heading_id)\
            .order_by(lbl_oc.desc(), lbl_title, lbl_section)

        if heading:
            q = q.filter(lbl_section == heading)
            return q

        return q

    @classmethod
    def q_wanted_pages(cls, session):
        p_by_l = Page.query_by_links(session)\
            .subquery("pages_by_links")

        lbl_oc = p_by_l.c.occurrences
        lbl_name = p_by_l.c.name
        q = session.query(p_by_l.c.id, lbl_oc, lbl_name)\
            .filter(not_(p_by_l.c.visited))\
            .order_by(lbl_oc.desc(), lbl_name)

        return q


def main():
    db = Model.sqlite("prueba.db")

    s = db.session()
    with db.session_scope() as s:
        s.query(TemplateCache).delete()
        t = TemplateCache(name="uno", body="cuerpo_uno")
        s.add(t)

        print("*" * 5)
        for row in s.query(TemplateCache):
            print(row)
        print("*" * 5)

        s.add(TemplateCache(name="dos", redirect_name="uno"))
        s.add(TemplateCache(name="tres", redirect_name="dos"))

        print("*" * 5)
        for row in s.query(TemplateCache):
            print(row)
        print("*" * 5)


if __name__ == "__main__":
    main()
