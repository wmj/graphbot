# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import

try:
    from functools import lru_cache
except ImportError:
    from functools32 import lru_cache

from wikiexpand.expand.templates import TemplateStore

from .model import TemplateCache


class TemplateDB(TemplateStore):

    MAX_REDIRECTIONS = 10

    def __init__(self, session, *args, **kwargs):
        super(TemplateDB, self).__init__(*args, **kwargs)
        self.session = session

    def _set(self, key, value):
        self.__clear_cache()
        row = TemplateCache(name=key, body=value, redirect_name=None)
        self.session.merge(row)

    def _get(self, key):
        row = self.target(key)

        if not row:
            raise KeyError(key)

        return row.body

    def target(self, key):
        row = self.session.query(TemplateCache).\
            filter_by(name=key).one_or_none()
        loop = 0

        while row and row.redirection and (loop <= self.MAX_REDIRECTIONS):
            row = row.redirection
            loop += 1

        return row

    def redirect(self, name_from, name_to):
        self.__clear_cache()
        row = TemplateCache(name=name_from, body=None)
        row.redirection = TemplateCache(name=name_to)

        self.session.merge(row)

    def clear(self):
        self.session.query(TemplateCache).delete()

    @lru_cache(maxsize=512)
    def parse(self, key, skip_style_tags=False):
        return super(TemplateDB, self).parse(key, skip_style_tags)

    def __clear_cache(self):
        return self.parse.cache_clear()
