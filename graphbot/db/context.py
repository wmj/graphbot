#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

import sys
from collections import deque
import struct
try:
    from functools import lru_cache
except ImportError:
    from functools32 import lru_cache

#import profilestats

from sqlalchemy import select, func, or_, and_
import sqlalchemy.sql.expression as sql
#from sqlalchemy.orm import aliased
from datetime import datetime

from wikiexpand.compat import iteritems
from wikiexpand.wiki import context
from wikiexpand.wiki.xml_dump import xml_generator
from . import model
from .templates import TemplateDB
from ..confusable import ConfusableHash


NAMESPACES = (0, 10, 4)


class Progress(object):
    CONTEXT = "context"
    CONTEXT_INC = "incremental_context"
    TRANSCLUSION = "transclusion"
    TIMESTAMP = "timestamp"
    COLLECTION = "collection"
    CONFUSABLES = "confusables"


class PreconditionException(Exception):
    pass


class UnexpectedLastOp(PreconditionException):

    def __init__(self, found, expected):
        super(UnexpectedLastOp, self).__init__(
            "Unexpected last operation '%s'"
            " (expected '%s')" % (found, expected)
        )


def print0(*args):
    print(*args, file=sys.stderr, end="")
    sys.stderr.flush()


def print_counter(counter):
    return " ".join("[ns %d: %d]" % it for it in iteritems(counter))


def _nop():
    pass


class DbWiki(context.Wiki):

    def __init__(self, session, *args, **kwargs):
        super(DbWiki, self).__init__(*args, **kwargs)
        self.session = session
        self.namespaces = tuple(self.namespace_normalize(code)
                                for code in NAMESPACES)

    @lru_cache(maxsize=512)
    def page_exists(self, title):
        _title = self.canonical_title(title)
        q = self.session.query(model.Page.id).filter_by(name=_title).filter(model.Page.lastedit.isnot(None))

        return q.scalar() is not None

    def status(self):
        return model.Status.singleton(self.session)

    def _pre_incremental(self):
        print(file=sys.stderr)
        # remove templatecache
        q = model.TemplateCache.__table__.delete()
        print0("- Cleaning template cache... ")
        res = self.session.execute(q)
        print("%d templates" % res.rowcount, file=sys.stderr)

        # remove page_inc
        q = model.PageInc.__table__.delete()
        print0("- Cleaning temporary table for incremental exploration... ")
        res = self.session.execute(q)
        print("%d pages" % res.rowcount, file=sys.stderr)

        self.session.flush()

    def _post_incremental(self):
        t_inc = model.PageInc.__table__
        t_pages = model.Page.__table__
        t_links = model.Link.__table__
        t_section_links = model.SectionLink.__table__
        print(file=sys.stderr)

        # flag is_new
        print0("- Identifying new pages... ")
        res = model.PageInc.query_update_is_new(self.session)
        print("%d new pages" % res.rowcount, file=sys.stderr)
        #c_new = res.rowcount

        # update flag visited
        q1 = t_pages.update()\
            .values(visited=False)\
            .values(xmlorder=None)\
            .values(xmlid=None)\
            .where(t_pages.c.visited)
        print("- Updating common pages... ", file=sys.stderr)
        print0("\t* Resetting flag 'visited'... ")
        res = self.session.execute(q1)
        print("%d old pages" % res.rowcount, file=sys.stderr)

        # update existing pages
        q_inc = self.session.query(model.PageInc.name)
        q3_sel_xor = select([t_inc.c.xmlorder])\
            .where(t_inc.c.name == t_pages.c.name)
        q3_sel_xid = select([t_inc.c.xmlid])\
            .where(t_inc.c.name == t_pages.c.name)
        q3_sel_lastedit = select([t_inc.c.lastedit])\
            .where(t_inc.c.name == t_pages.c.name)
        q2 = t_pages.update()\
            .values(visited=True)\
            .values(xmlorder=q3_sel_xor)\
            .values(xmlid=q3_sel_xid)\
            .values(lastedit=q3_sel_lastedit)\
            .where(t_pages.c.name.in_(q_inc))
        print0("\t* Updating timestamp & flag 'visited'... ")
        res = self.session.execute(q2)
        print(res.rowcount, file=sys.stderr)
        #c_common = res.rowcount

        # TODO para crear el índice hay que almacenar el nuevo lastedit

        # q3_sel = select([t_inc.c.lastedit])\
        #     .where(t_inc.c.name == t_pages.c.name)
        # q3 = t_pages.update()\
        #     .values(lastedit=q3_sel)\
        #     .where(t_pages.c.namespace == 10)
        # print0("* Updating timestamp... ")
        # res = self.session.execute(q3)
        # print(res.rowcount, file=sys.stderr)

        # insert new pages
        q4_sel = select([t_inc.c.name,
                         t_inc.c.lastedit,
                         t_inc.c.namespace,
                         True,
                         t_inc.c.xmlid,
                         t_inc.c.xmlorder])\
            .where(t_inc.c.is_new)
        #     .where(t_inc.c.is_new)\
        #     .where(t_inc.c.namespace != 0)
        q4 = t_pages.insert()\
            .from_select([
                'name', 'lastedit', 'namespace',
                'visited', 'xmlid', 'xmlorder'
            ], q4_sel)
        print0("- Inserting new pages... ")
        res = self.session.execute(q4)
        print(res.rowcount, file=sys.stderr)
        #n = res.rowcount
        #assert res.rowcount == c_new

        # q40_sel = select([t_inc.c.name,
        #                   t_inc.c.namespace,
        #                   True])\
        #     .where(t_inc.c.is_new)
        # q40 = t_pages.insert()\
        #     .from_select(['name', 'namespace', 'visited'], q40_sel)
        # # XXX
        # res = self.session.execute(q40)

        # remove links from deleted pages
        q5_sel = self.session.query(model.Page.id).filter(model.Page.visited)
        q5 = t_links.delete()\
            .where(t_links.c.from_id.notin_(q5_sel))
        print0("\t* Purging links from deleted pages... ")
        res = self.session.execute(q5)
        print(res.rowcount, file=sys.stderr)

        # remove section_links from deleted pages
        q6 = t_section_links.delete()\
            .where(t_section_links.c.from_id.notin_(q5_sel))
        print0("\t* Purging section links from deleted pages... ")
        res = self.session.execute(q6)
        print(res.rowcount, file=sys.stderr)

        # remove not visited pages who are not link targets
        q0_to = self.session.query(model.Link.to_id)
        q0 = t_pages.delete()\
            .where(t_pages.c.namespace == 0)\
            .where(sql.not_(t_pages.c.visited))\
            .where(t_pages.c.id.notin_(q0_to))
        print0("\t* Purging unproductive pages... ")
        res = self.session.execute(q0)
        print("%d deleted pages" % res.rowcount, file=sys.stderr)

        # remove pages_inc
        #print0("\t* Cleaning temporary table for incremental exploration... ")
        #res = self.session.execute(t_inc.delete())
        #print(res.rowcount, file=sys.stderr)
        #assert res.rowcount == c_new + c_common

        self.session.flush()

    def build_context(
            self,
            dump_name,
            commit=True,
            cache_size=4096,
            info_step=512,
            incremental=False):

        status = self.status()

        dump_lastedit = None

        # check precondition
        if incremental:
            if status.lastop != Progress.CONFUSABLES:
                raise UnexpectedLastOp(status.lastop, Progress.CONFUSABLES)
            if status.is_context(dump_name):
                raise PreconditionException(
                    "context already built: %s" % dump_name)
            if not status.finished:
                raise PreconditionException(
                    "previous collection not finished: %s" % status.dump)

            db_table = model.PageInc
            self._pre_incremental()

            # ensure previous dump's last edit is stored
            if not status.lastedit:
                dump_lastedit = model.Page.get_last_edit(self.session)
            else:
                dump_lastedit = status.lastedit
        else:
            db_table = model.Page
            if not status.is_empty() or status.lastop:
                raise PreconditionException(
                    "context is not empty"
                    " and incremental mode is not set")

        if commit:
            do_commit = self.session.flush
        else:
            do_commit = _nop

        T = TemplateDB(self.session)

        do_commit()

        counter = {x: 0 for x in NAMESPACES}

        print("\nBuilding context for '%s':" % dump_name, file=sys.stderr)

        pages = [None] * cache_size
        for idx in range(cache_size):
            pages[idx] = {"visited": True}

        step_idx = 0

        for idx, (cnt, page) in enumerate(xml_generator(dump_name, NAMESPACES)):

            counter[page.ns] += 1

            title = (page.title
                     if page.ns == 0
                     else self.canonical_title(page.title))

            c_page = pages[step_idx]
            c_page["name"] = title
            if incremental and (page.ns == 0):
                c_page["lastedit"] = page.timestamp
            elif "lastedit" in c_page:
                del c_page["lastedit"]
            c_page["namespace"] = page.ns
            page_id = int(page.id)
            assert page_id
            c_page["xmlid"] = page_id
            c_page["xmlorder"] = idx

            if page.ns == 10:
                # template
                c_page["lastedit"] = page.timestamp
                title = page.clean_title
                if page.isredirect:
                    target = self.canonical_title(page.redirect_target)
                    T.redirect(title, target)
                else:
                    # XXX
                    if title.endswith("/doc"):
                        page.text = ""
                    T[title] = page.text
                do_commit()

            step_idx += 1

            if step_idx == cache_size:
                self.session.bulk_insert_mappings(db_table, pages)
                do_commit()
                step_idx = 0

            if idx % info_step == 0:
                sys.stderr.write("\r%d (%d) " % (idx, cnt))
                sys.stderr.write(print_counter(counter))
                sys.stderr.flush()

        if step_idx > 0:
            self.session.bulk_insert_mappings(db_table, pages[:step_idx])

        sys.stderr.write("\n")
        sys.stderr.flush()

        # touch status
        #status = model.Status.singleton(self.session)
        status.reset(dump_name)
        status.size = cnt
        status.incremental = incremental
        if dump_lastedit:
            status.lastedit = dump_lastedit
        status.lastop = (Progress.CONTEXT_INC
                         if incremental
                         else Progress.CONTEXT)

        self.session.merge(status)

        if incremental:
            self._post_incremental()

        self.session.commit()

        print("\nContext successfully built.", file=sys.stderr)

    #@profilestats.profile()
    def collect_transclusions(
            self,
            dump_name,
            cache_size=4096,
            commit=True,
            info_step=64):

        #t_trans = model.Transclusion.__table__.insert()

        session = self.session
        if commit:
            do_commit = self.session.flush
        else:
            do_commit = _nop

        status = self.status()
        size = status.size

        if status.is_empty():
            raise PreconditionException("context not built")
        if not status.is_context(dump_name):
            raise PreconditionException("context is built for another dump: %s" % status.dump)
        if status.finished:
            raise PreconditionException("collection has already finished")
        if status.is_collecting():
            raise PreconditionException("collection is on (from page '%s'" % status.page_from)
        if session.query(model.Transclusion).limit(1).count() > 0:
            raise PreconditionException("transclusion table is not empty")
        if status.lastop != Progress.CONTEXT_INC:
            raise UnexpectedLastOp(status.lastop, Progress.CONTEXT_INC)

        NS = (0, 10)

        counter = {x: 0 for x in NS}

        cache = deque()

        ti = _TemplateIndexer(self)

        ss = 0

        for idx, (cnt, page) in enumerate(xml_generator(dump_name, NS)):
            title = (page.title if page.ns == 0
                     else self.canonical_title(page.title))

            counter[page.ns] += 1

            page_id = session.query(model.Page.id).\
                filter_by(name=title).scalar()

            if not page_id:
                continue

            templates = [x for x in self.transcluded_templates(ti.body(page))
                         if not x.endswith("/doc")]

            if not templates:
                continue

            template_ids = set(ti.get(t_name) for t_name in templates)

            cache.extend({"page_id": page_id, "template_id": tid}
                         for tid in template_ids
                         if tid is not None and page_id != tid)

            # insert existing collect_transclusions
            csize = len(cache)
            if len(cache) >= cache_size:
                session.bulk_insert_mappings(model.Transclusion, cache)
                do_commit()
                ss += csize
                cache.clear()

            if idx % info_step == 0:
                sys.stderr.write("\r%0.2f%% %d (%d) " %
                                 (100.0 * cnt / size, idx, cnt))
                sys.stderr.write(print_counter(counter))
                sys.stderr.flush()

        ss += len(cache)

        print("size", ss)

        status.lastop = Progress.TRANSCLUSION
        session.merge(status)

        session.commit()
        #session.rollback()

        print("\nTransclusion successfully collected.", file=sys.stderr)

    def propagate_timestamp(self):

        # check precondition

        status = self.status()
        if status.is_empty():
            raise PreconditionException("context is empty")
        if status.finished:
            raise PreconditionException("collection has finished")
        if status.is_collecting():
            raise PreconditionException("collection has begun")
        if self.session.query(model.Transclusion).limit(1).count() <= 0:
            raise PreconditionException("transclusion table is empty")
        if status.lastop != Progress.TRANSCLUSION:
            raise UnexpectedLastOp(status.lastop, Progress.TRANSCLUSION)

        QUERY = """
UPDATE pages
SET ext_lastedit=
    MAX(
        ext_lastedit,
        (SELECT MAX(p2.ext_lastedit)
         FROM pages p2
         INNER JOIN transclusion t1
         ON p2.id = t1.template_id
         WHERE
            p2.lastedit IS NOT NULL
            AND
            t1.page_id = pages.id
            AND
            t1.template_id NOT IN
                (SELECT t2.page_id FROM transclusion t2)
        )
       )
WHERE
 lastedit IS NOT NULL
 AND
 id IN
 (
  SELECT page_id FROM transclusion
  WHERE template_id NOT IN (SELECT page_id FROM transclusion)
 )
"""
        t_pages = model.Page.__table__
        t_trans = model.Transclusion.__table__

        # a deleted template force the timestamp updating
        q_deleted = t_pages.update()\
            .where(sql.not_(t_pages.c.visited))\
            .where(t_pages.c.namespace == 10)\
            .values(lastedit=datetime.now())
        print0("\n- Marking deleted templates... ")
        res = self.session.execute(q_deleted)
        print(res.rowcount, file=sys.stderr)

        # set initial ext_lastedit
        q = t_pages.update()\
            .values(ext_lastedit=t_pages.c.lastedit)\
            .where(t_pages.c.lastedit.isnot(sql.null()))
        print0("- Initializing extended lastedit... ")
        res = self.session.execute(q)
        print(res.rowcount, file=sys.stderr)

        qd_sel = self.session.query(model.Transclusion.page_id)

        qd = t_trans.delete().\
            where(t_trans.c.template_id.notin_(qd_sel))

        depth = 0
        are_leaf_nodes = True
        while are_leaf_nodes:
            print("\n- LEVEL %d" % depth, file=sys.stderr)
            print0("\t* propagating timestamp from leaves... ")
            res = self.session.execute(QUERY)
            print(res.rowcount, file=sys.stderr)

            # prune leaf nodes
            print0("\t* pruning leaf nodes... ")
            res = self.session.execute(qd)
            print(res.rowcount, file=sys.stderr)

            are_leaf_nodes = res.rowcount > 0
            depth += 1

        # remove deleted auxiliar pages
        q7 = t_pages.delete()\
            .where(sql.not_(t_pages.c.visited))\
            .where(t_pages.c.namespace != 0)
        print0("\t* Purging deleted auxiliar pages... ")
        res = self.session.execute(q7)
        print(res.rowcount, file=sys.stderr)

        self._clear_revisited()

        print("Timestamp propagation finished", file=sys.stderr)

        status.lastop = Progress.TIMESTAMP
        self.session.merge(status)

        self.session.commit()

    def skip_ratio(self):

        # join page / pageinc en ns 0

        t_page = model.Page.__table__
        t_inc = model.PageInc.__table__

        has_inc = self.session.query(model.PageInc.name).limit(1).count()

        t_previous_dump = self.status().lastedit
        if has_inc:

            condition = or_(
                t_page.c.lastedit == sql.null(),
                and_(
                    t_page.c.ext_lastedit > t_previous_dump,
                    t_page.c.lastedit < t_page.c.ext_lastedit
                ),
                t_inc.c.lastedit > t_previous_dump
            )
#                t_inc.c.lastedit > func.max(t_page.c.lastedit,
#                                            t_page.c.ext_lastedit)

            join = t_page.join(t_inc, t_page.c.name == t_inc.c.name)

            q = select([func.count("*")])\
                .select_from(join)\
                .where(t_page.c.namespace == 0)\
                .where(t_page.c.visited)\
                .where(condition)

        else:
            if t_previous_dump:
                condition = or_(
                    t_page.c.lastedit == sql.null(),
                    and_(
                        t_page.c.ext_lastedit > t_previous_dump,
                        t_page.c.lastedit < t_page.c.ext_lastedit
                    )
                )
            else:
                condition = or_(
                    t_page.c.lastedit == sql.null(),
                    t_page.c.lastedit < t_page.c.ext_lastedit
                )
            q = select([func.count("*")])\
                .select_from(t_page)\
                .where(t_page.c.namespace == 0)\
                .where(t_page.c.visited)\
                .where(condition)

        need_update = self.session.scalar(q)
        q_total = select([func.count("*")])\
            .select_from(t_page)\
            .where(t_page.c.namespace == 0)\
            .where(t_page.c.visited)
        total = self.session.scalar(q_total)

        return 1.0 - (need_update / total), need_update, total

    def generate_index(self):

        status = self.status()

        if status.lastop not in (Progress.TIMESTAMP,):
            raise UnexpectedLastOp(status.lastop, Progress.TIMESTAMP)

        print("Building index...\n", file=sys.stderr)

        skip_ratio, affected, total = self.skip_ratio()

        print("Index size: %d / %d (%0.2f%%)" % (affected, total,
              100.0 * affected / total), file=sys.stderr)

        t_previous_dump = status.lastedit
        assert t_previous_dump

        t_page = model.Page.__table__

        condition = or_(
            t_page.c.lastedit == sql.null(),
            and_(
                t_page.c.ext_lastedit > t_previous_dump,
                t_page.c.lastedit < t_page.c.ext_lastedit
            )
        )
        q = select([t_page.c.xmlid])\
            .select_from(t_page)\
            .where(t_page.c.namespace == 0)\
            .where(t_page.c.visited)\
            .where(condition)\
            .order_by(t_page.c.xmlorder)

        res = self.session.execute(q)

        # abrir archivo
        filename = status.dump
        assert filename
        filename = "%s.idx" % filename

        print("Writing file '%s' ..." % filename, file=sys.stderr)

        fmt = struct.Struct("I")

        step = 512

        def plog0(idx, affected):
            print0("\r{:d} / {:d} ({:0.2f}%%)".format(
                idx, affected,
                100.0 * idx / affected
            ))

        print0("0 / %d (0.00%%)", total)
        with open(filename, "wb") as fd:
            for idx, (xid,) in enumerate(res, start=1):
                # escribir integer
                fd.write(fmt.pack(xid))
                if idx % step == 0:
                    plog0(idx, affected)

        plog0(idx, affected)
        print("\n\nDone\n", file=sys.stderr)

        status.lastop = Progress.CONTEXT
        self.session.merge(status)
        self.session.commit()

    def _clear_revisited(self):

        t_page = model.Page.__table__
        t_inc = model.PageInc.__table__

        t_previous_dump = self.status().lastedit

        print("LAST EDIT", t_previous_dump, file=sys.stderr)

        # it is new
        # or
        # transcluded content has changed (after last dump) TODO
        # or
        # page in new dump is newer
        condition = or_(
            t_page.c.lastedit == sql.null(),
            and_(
                t_page.c.ext_lastedit > t_previous_dump,
                t_page.c.lastedit < t_page.c.ext_lastedit
            ),
            t_inc.c.lastedit > t_previous_dump
        )
#            t_inc.c.lastedit > func.max(t_page.c.lastedit,
#                                        t_page.c.ext_lastedit)

        join = t_page.join(t_inc, t_page.c.name == t_inc.c.name)

        q = select([t_page.c.id])\
            .select_from(join)\
            .where(t_page.c.namespace == 0)\
            .where(t_page.c.visited)\
            .where(condition)

        #need_update = self.session.scalar(q)

        qu = t_page.update()\
            .where(t_page.c.id.in_(q))\
            .values(lastedit=None)

        print0("\t* Resetting revisited pages... ")
        res = self.session.execute(qu)
        print(res.rowcount, file=sys.stderr)

        q_re = self.session.query(model.Page.id)\
            .filter_by(lastedit=None)\
            .filter_by(visited=True)

        t_section = model.Section.__table__
        t_slink = model.SectionLink.__table__
        t_link = model.Link.__table__

        q1 = t_section.update()\
            .where(t_section.c.page_id.in_(q_re))\
            .values(blue=False)

        print0("\t* Removing headings in revisited pages... ")
        res = self.session.execute(q1)
        print(res.rowcount, file=sys.stderr)

        q2 = t_slink.delete()\
            .where(t_slink.c.from_id.in_(q_re))

        print0("\t* Removing section links in revisited pages... ")
        res = self.session.execute(q2)
        print(res.rowcount, file=sys.stderr)

        q3 = t_link.delete()\
            .where(t_link.c.from_id.in_(q_re))

        print0("\t* Removing links in revisited pages... ")
        res = self.session.execute(q3)
        print(res.rowcount, file=sys.stderr)

        # remove pages_inc
        print0("\t* Cleaning temporary table for incremental exploration... ")
        res = self.session.execute(t_inc.delete())
        print(res.rowcount, file=sys.stderr)

        #self.session.rollback()

    def collect_confusables(self, cache_size=4096):
        status = self.status()
        if (status.lastop not in (Progress.COLLECTION, Progress.CONFUSABLES)):
            if (status.lastop == Progress.COLLECTION) and not status.finished:
                raise PreconditionException("collection is in progress")
            raise UnexpectedLastOp(status.lastop, Progress.COLLECTION)

        H = ConfusableHash()

        session = self.session

        q_base = session.query(model.UnicodeHash.page_id)

        n_pages = session.query(model.func.count("*"))\
            .filter(model.Page.namespace == 0)\
            .filter(model.Page.id.notin_(q_base))\
            .scalar()

        # pages in ns:0 with no hash
        #t_pages = model.Pages.__table__
        q_sel = session.query(model.Page.id, model.Page.name)\
            .filter_by(namespace=0)\
            .filter(model.Page.id.notin_(q_base))

        data = [None] * cache_size
        for idx in range(cache_size):
            data[idx] = {"page_id": None, "key": None}

        print("Generating unicode hashes...\n\n", file=sys.stderr)

        cache_idx = 0

        fmt = "\r%d/%d (%0.2f%%)"

        for idx, (id, name) in enumerate(q_sel, start=1):
            for x in H.hash_strings(name):
                if cache_idx == cache_size:
                    session.bulk_insert_mappings(model.UnicodeHash, data)
                    session.flush()
                    cache_idx = 0
                el = data[cache_idx]
                el["page_id"] = id
                el["u_key"] = x
                cache_idx += 1

            if idx % 1024 == 0:
                sys.stderr.write(fmt % (
                    idx,
                    n_pages,
                    100.0 * idx / n_pages
                ))

        sys.stderr.write(fmt % (
            idx,
            n_pages,
            100.0 * idx / n_pages
        ))

        if cache_idx > 0:
            session.bulk_insert_mappings(model.UnicodeHash, data[:cache_idx])
            session.flush()

        status.lastop = Progress.CONFUSABLES
        session.merge(status)
        session.flush()

        print()

    def page_confusables(self, title, heading=None):

        session = self.session

        p = session.query(model.Page).filter_by(name=title).one_or_none()

        h = None
        if heading:
            h = session.query(model.Heading)\
                .filter_by(name=heading)\
                .one_or_none()

        if p:
            q = (p.confusables(session)
                 if not h
                 else p.section_confusables(session, h))
            for d in q:
                yield d.name

    def insert_language_codes(self, langcode_iterable, replace=True):

        session = self.session

        data = [{"code": code, "name": name}
                for code, name in langcode_iterable]

        if replace:
            self.session.query(model.LanguageCode).delete()
            self.session.flush()

        self.session.bulk_insert_mappings(model.LanguageCode, data)
        self.session.flush()


class _TemplateIndexer(object):

    def __init__(self, context):
        self.context = context
        self.session = context.session
        self.T = TemplateDB(self.session)
        self.q = context.session.query(model.Page.id)
        self.str_template = self.context.namespace_name("Template") + ":%s"

    @lru_cache(maxsize=2048)
    def get(self, template_name):
        title = self.context.clean_title(template_name)
        row = self.T.target(title)
        if row:
            template_name = self.str_template % row.name

        return self.q.filter_by(name=template_name).scalar()

    def body(self, page):
        if page.ns == 10:
            return self.T.parse(page.title.split(":", 1)[1])
        return page.text
